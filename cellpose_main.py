import argparse
import csv
import logging
import os
import shutil
import sys
from pathlib import Path

import cv2
import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from cellpose import io, metrics, models, utils
from cellpose.train import train_seg
from scipy.stats import entropy
from skimage import measure
from torch.utils.data import Dataset

def count_contours(mask):
    mask_binary = np.uint8(mask > 0)
    contours, _ = cv2.findContours(mask_binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return len(contours)
class MyDataset(Dataset):
    """
    A PyTorch Dataset to load masks and images from file paths

    ...
    Attributes
    ----------
    input_dir : str
        the root directory containing the images and masks folders
    transform : callable
        a function/transform to apply to each image

    Methods
    -------
    __getitem__(idx)
        returns the image, image path, and masks for the given index
    get_single_mask(masks)
        dissects a image with multiple masks into multiple images with one mask each
    """

    def __init__(self, input_dir, transform=None):
        self.input_dir = input_dir
        self.transform = transform
        # get all images from input_dir
        self.images = os.listdir(os.path.join(input_dir, "imgs"))
        self.masks = [img.replace(".jpg", "_mask.png") for img in self.images]
        # get all masks from input_dir
        # self.masks = os.listdir(os.path.join(input_dir, "masks"))

    def __len__(self):
        return len(self.images)

    def __getitem__(self, idx):
        image_path = self.images[idx]
        image = cv2.imread(os.path.join(self.input_dir, "imgs", image_path))
        # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        mask_path = self.masks[idx]
        mask = cv2.imread(os.path.join(self.input_dir, "masks", mask_path))

        if self.transform:
            image, mask = self.transform(image, mask)

        return image, mask, image_path

    def get_single_mask(self, masks):
        """creates a single image with one mask per image
        ARGS:
            masks (Tensor): list of masks
        RETURNS:
            single_masks (List[Tensor]): all masks in one image

        """
        # TODO
        pass

    def get_dataset_as_list(self):
        """returns a list of all images and masks in the dataset
        RETURNS:
            dataset Tuple,([List[np.ndarray], List[np.ndarray]): list of images and masks
        """
        image_list = []
        mask_list = []
        for image, mask in zip(self.images, self.masks):
            logger.info(f"image: {image},\n mask: {mask}")
            cur_img = cv2.imread(os.path.join(self.input_dir, "imgs", image))
            # make image grayscale and convert to numpy array
            cur_img = cv2.cvtColor(cur_img, cv2.COLOR_BGR2GRAY)
            image_list.append(cur_img)
            if not os.path.exists(os.path.join(self.input_dir, "masks", mask)):
                logger.warning(f'mask: {mask} not found')

                mpath = os.path.join(self.input_dir, "masks", mask)
                logger.warning(f'checking in {mpath}')
                alt_path  = mpath.replace("pool","")
                shutil.move(alt_path, mpath)
            cur_mask = cv2.imread(os.path.join(self.input_dir, "masks", mask))
            # make mask grayscale and convert to numpy array
            cur_mask = cv2.cvtColor(cur_mask, cv2.COLOR_BGR2GRAY)
            mask_list.append(cur_mask)
        return image_list, mask_list


def save_al_scores(active_learning_dict):
    if not os.path.exists(os.path.join(outpath)):
        os.makedirs(os.path.join(outpath))
    logger.info(f"Saving active learning scores to {outpath}/active_learning.csv")
    with open(os.path.join(outpath, "active_learning.csv"), "w") as f:
        # use keys as header for  csv columns
        w = csv.DictWriter(f, active_learning_dict.keys())
        w.writeheader()
        for i in range(len(active_learning_dict["filename"])):
            w.writerow(
                {
                    "filename": active_learning_dict["filename"][i],
                    "score": active_learning_dict["score"][i],
                    "entropy": active_learning_dict["entropy"][i],
                    "maskpath": active_learning_dict["maskpath"][i],
                }
            )


def visualize_instances(image_path_base, pred_mask, flag, input_dir):
    # Load the image and binary mask
    mask_base = image_path_base.replace(".jpg", "_mask.png")
    result_file = image_path_base.replace(".jpg", "_overlay.png")
    image_path = os.path.join(input_dir, "imgs", image_path_base)
    mask_path = os.path.join(input_dir, "masks", mask_base)
    image = cv2.imread(image_path)
    mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)
    # Find contours in the binary mask
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    countours_pred, _ = cv2.findContours(
        pred_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
    )
    # Draw contours on the original image
    image_with_contours = image.copy()
    image_with_pred_contours = image.copy()
    cv2.drawContours(image_with_contours, contours, -1, (0, 255, 0), 2)
    cv2.drawContours(image_with_pred_contours, countours_pred, -1, (0, 255, 0), 2)
    # Plot the images side by side
    plt.subplot(1, 2, 1)
    plt.imshow(cv2.cvtColor(image_with_contours, cv2.COLOR_BGR2RGB))
    plot_name = image_path_base.replace("Aufnahme", "")
    plt.title(f"Image GT:\n {plot_name}")
    plt.subplot(1, 2, 2)
    plt.imshow(cv2.cvtColor(image_with_pred_contours, cv2.COLOR_BGR2RGB))
    plt.title("Image with Prediction")
    # plot original mask next to prediction overlay
    if not os.path.exists(os.path.join(f"{outpath}", flag, "plots")):
        Path(os.path.join(f"{outpath}", flag, "plots")).mkdir(
            parents=True, exist_ok=True
        )
        logger.info(f"created {outpath}/{flag}/plots directory")

    plt.savefig(
        os.path.join(f"{outpath}", flag, "plots", result_file),
        bbox_inches="tight",
        dpi=300,
    )
    plt.close()


def visualize_mask_img_pair(image, mask, path, flag):
    plt.figure(figsize=(10, 10))
    plt.subplot(1, 2, 1)
    plt.title(f"Image: {path}")
    plt.imshow(image, cmap="gray")
    plt.subplot(1, 2, 2)
    plt.imshow(mask, cmap="gray")
    out_basepth = os.path.join(f"{outpath}", "plots", "input_vis")
    if not os.path.exists(out_basepth):
        Path(out_basepth).mkdir(parents=True, exist_ok=True)
        logger.info(f"created {out_basepth} directory")
    if "mask" in path:
        path = path.replace("_mask.png", "jpg")
    path = path.replace("jpg", "_vis.png")
    plt.savefig(os.path.join(out_basepth, path), bbox_inches="tight", dpi=300)
    plt.close()



def calculate_mean_diameters(masks):
    horizontal_diameters = []
    vertical_diameters = []

    # Iterate through each mask
    for mask in masks:
        # Find contours of the mask
        contours = measure.find_contours(mask, 0.5)

        # Calculate the diameter using the maximum distance between points in the contour
        if len(contours) > 0:
            #contour = contours[0]
            for contour in contours:
                y, x = contour.T
                dx = x[:, np.newaxis] - x
                dy = y[:, np.newaxis] - y
                horizontal_distances = np.abs(dx)
                vertical_distances = np.abs(dy)
                horizontal_diameter = np.max(horizontal_distances)
                vertical_diameter = np.max(vertical_distances)
                horizontal_diameters.append(horizontal_diameter)
                vertical_diameters.append(vertical_diameter)

    # Calculate the mean diameters
    mean_horizontal_diameter = np.mean(horizontal_diameters)
    mean_vertical_diameter = np.mean(vertical_diameters)
    return mean_horizontal_diameter, mean_vertical_diameter


def get_iou(gt_mask, pred_mask):
    """Compute the intersection sum and union sum of two masks
    Args:
        gt_mask: ground truth mask
        pred_mask: predicted mask
    Returns:
        union_sum: sum of union of two masks
        intersection_sum: sum of intersection of two masks

    """
    intersection = np.logical_and(gt_mask, pred_mask)
    union = np.logical_or(gt_mask, pred_mask)
    union_sum = np.sum(union)
    intersection_sum = np.sum(intersection)

    return union_sum, intersection_sum


def preprocess_masks(masks):
    """
    makes sure masks are binary uint8 images

    ARGS:
        masks (List[np.ndarray]): list of masks
    RETURNS:
        masks (List[np.ndarray]): list of masks

    """
    out = []
    for m in masks:
        m = m.astype(np.uint8)
        m = m > 0
        out.append(m)
    return out


def criterion(pred_masks, gt_masks):
    """
    Compute the score for the predicted masks
    ARGS:
        pred_masks (List[np.ndarray]): list of predicted masks
        gt_masks (List[np.ndarray]): list of ground truth masks
    RETURNS:
        score (float): the score for the predicted masks
    """
    union, intersection = 0., 0.
    for pred_mask, gt_mask in zip(pred_masks, gt_masks):
        u, i = get_iou(gt_mask, pred_mask)
        union += u
        intersection += i
    return intersection / union


def find_optimal_threshold_combi(gt_masks, model, my_diam_mean):
    """
    Find the optimal flow and mask thresholds for the model based on IoU score
    ARGS:
        gt_masks (List[np.ndarray]): list of ground truth masks
        mask_list (List[np.ndarray]): list of masks
        model (CellposeModel): the model to evaluate
        my_diam_mean (float): the mean diameter of the cells
    RETURNS:
        optimized_cellpose_threshold (float): the best cellprob threshold
        optimized_flow_treshhold (float): the best flow threshold

    """
    best_score = -np.inf
    flow_tresholds = np.arange(0, 3, 0.5).tolist()
    cellprob_thresholds = np.arange(-6, 6, 0.5).tolist()
    best_thresholds = {"flow_threshold": 0.0, "cellprob_threshold": 0.0}
    n_gt_masks = []
    for gt in gt_masks:
        n_gt_masks.append(count_contours(gt))

    for ft in flow_tresholds:
        for ct in cellprob_thresholds:
            print(f'current combination ft:{ft}, ct:{ct}')
            pred_masks, *_ = model.eval(
                gt_masks,
                channels=[0, 0],
                diameter=my_diam_mean,
                cellprob_threshold=ct,
                flow_threshold=ft,
                min_size=5
            )
            pred_masks = preprocess_masks(pred_masks)
            print(f'number pred images: {len(pred_masks)}')
            delta_sum = 0
            for i, m in enumerate(pred_masks):
                n_pred_masks = count_contours(m)

                delta_sum+= abs(n_pred_masks-n_gt_masks[i])/ n_gt_masks[i]
                penality = -1000 if n_pred_masks == 0 else 0
            print(f'cur delta: {delta_sum}')
            print(f'penalty: {penality}')
            iou = criterion(pred_masks=pred_masks, gt_masks=gt_masks)
            print(f'iou {iou}')
            score = iou + penality # + delta_sum
            if score > best_score:
                best_thresholds["flow_treshold"] = ft
                best_thresholds["cellprob_threshold"] = ct
                best_score = score
                print('best_score',best_score)
            score = 0

    return best_thresholds["cellprob_threshold"], best_thresholds["flow_treshold"]

def main():
    train_dataset = MyDataset(root_dir)
    train_image_list, mask_list = train_dataset.get_dataset_as_list()
    train_image_list = [np.expand_dims(image, axis=0) for image in train_image_list]
    test_dataset = MyDataset(test_root)
    test_image_list, test_mask_list = test_dataset.get_dataset_as_list()
    test_image_list = [np.expand_dims(image, axis=0) for image in test_image_list]

    # make suitable to plot with matplotlib by reordering channels
    mean_horizontal_diameter, mean_vertical_diameter = calculate_mean_diameters(mask_list)
    my_diam_mean = (mean_vertical_diameter + mean_horizontal_diameter) / 2
    model = models.CellposeModel(gpu=usegpu, model_type='cyto', diam_mean=my_diam_mean)

    # add dimension to image_list and mask_list
    logger.info(f"training images: {len(train_image_list)}")
    logger.info(f" visualizing training images")
    for image, mask, path in zip(train_image_list, mask_list, train_dataset.images):
        image = np.moveaxis(image, 0, -1)
        # visualize_mask_img_pair(image, mask, path)
        visualize_instances(path, mask, "train_in", root_dir)

    if not os.path.exists(os.path.join(outpath, "models")):
        os.makedirs(os.path.join(outpath, "models"))
    if train:
        logger.info("starting training")
        model_path = train_seg(
            model.net,
            train_data=train_image_list,
            train_labels=mask_list,
            test_data=test_image_list,
            test_labels=test_mask_list,
            channels=[0, 0],
            batch_size=128,
            save_path=os.path.join(outpath),
            save_every=50,
            learning_rate=0.001,
            n_epochs=epochs,
            min_train_masks=1,
            model_name="model_final.pth"
        )

        logger.info(f'model_path after train: {model_path}')

    # in rare cases we will use the model without training (0shot learning), so we have the code below outside the if train block
    # the cellprob_threshold and the flow_treshold have a high impact on the IoU
    # thus, we find this values programatically to have the best IoU for our train set
    optimized_cellpose_threshold, optimized_flow_treshhold = find_optimal_threshold_combi(gt_masks=mask_list, model=model, my_diam_mean=my_diam_mean)
    masks, flows, *_ = model.eval(train_image_list, channels=[0,0], diameter=my_diam_mean, cellprob_threshold= optimized_cellpose_threshold,flow_threshold=optimized_flow_treshhold)
    for image, mask, path in zip(train_image_list, masks, train_dataset.images):
        mask = mask.astype(np.uint8)
        visualize_instances(path, mask, "train_pred", root_dir)

    if active_learning:
        logger.info("active learning flag is set to True")

        pool_root = os.path.join(root_dir, "pool")
        al_dataset = MyDataset(pool_root)
        pool_imgs, pool_masks = al_dataset.get_dataset_as_list()
        pool_imgs = [np.expand_dims(image, axis=0) for image in pool_imgs]
        logger.info(f"pool images: {len(pool_imgs)}")
        logger.info(f"visualizing pool images")
        for image, mask, path in zip(pool_imgs, pool_masks, al_dataset.images):
            image = np.moveaxis(image, 0, -1)
            # visualize_mask_img_pair(image, mask, path)
            mask = mask.astype(np.uint8)
            visualize_instances(path, mask, "pool_in", pool_root)
        # the cellprob output is the probability of a pixel being a cell, which
        # we need for active learning
        # this is explained in the docstring of the eval() function of the
        # cellpose model see:
        # flows: list of lists 2D arrays, or list of 3D arrays (if do_3D=True)
        #     flows[k][0] = XY flow in HSV 0-255
        #     flows[k][1] = XY flows at each pixel
        #     flows[k][2] = cell probability (if > cellprob_threshold, pixel used for dynamics)
        #     flows[k][3] = final pixel locations after Euler integration

        masks, flows, *_ = model.eval(pool_imgs, channels=[0,0], diameter=my_diam_mean, cellprob_threshold=optimized_cellpose_threshold, flow_threshold=optimized_flow_treshhold)

        active_learning_dict = {
            "filename": [],
            "score": [],
            "entropy": [],
            "maskpath": [],
        }

        for image, mask, path, flow in zip(pool_imgs, masks, al_dataset.images, flows):
            probs = flow[2]
            probs_sig = torch.sigmoid(torch.from_numpy(probs))
            image = np.moveaxis(image, 0, -1)
            mask = mask.astype(np.uint8)
            visualize_instances(path, mask, "pool_pred", pool_root)
            # save predictions as maskm
            mask_path = path.replace("jpg", "_pred_mask.png")
            in_mask_path = path.replace("jpg", "_mask.png")
            outfile = os.path.join(outpath, "pool_pred_masks", mask_path)
            cv2.imwrite(outfile, mask)

            # Active learning PART --------------------------------------------
            probs = flow[2]
            probs_sig = torch.sigmoid(torch.from_numpy(probs))

            outfile_probs = os.path.join(outpath, "pool_pred_probs", mask_path)
            cv2.imwrite(outfile_probs, probs)
            pred_binary = mask.copy()

            uncertainty = np.sum(np.power(pred_binary - probs_sig.cpu().numpy(), 2))
            entr = entropy(probs_sig)
            active_learning_dict["filename"].append(path)
            active_learning_dict["score"].append(uncertainty)
            active_learning_dict["entropy"].append(entr)
            active_learning_dict["maskpath"].append(in_mask_path)

        # save uncertainty scores to csv
        save_al_scores(active_learning_dict)

    if evaluate:

        logger.info(f'model pretrained: {model.pretrained_model}')
        masks, flows, styles = model.eval(test_image_list, channels=[0, 0], diameter=my_diam_mean, cellprob_threshold=optimized_cellpose_threshold, flow_threshold=optimized_flow_treshhold)


        logger.info("visualizing input test images")
        for image, mask, path in zip(
            test_image_list, test_mask_list, test_dataset.images
        ):
            image = np.moveaxis(image, 0, -1)
            mask = mask.astype(np.uint8)
            visualize_instances(path, mask, "test_in", test_root)

        # visualize predictions
        logger.info(f" visualizing predictions on test images")
        path = test_dataset.images[0]
        for image, mask, path in zip(test_image_list, masks, test_dataset.images):
            image = np.moveaxis(image, 0, -1)

            # cast to uint8 to be consistent with image
            mask = mask.astype(np.uint8)
            visualize_instances(path, mask, "test_pred", test_root)
            out_basepth = os.path.join(f"{outpath}", "test", "predictions")
            if not os.path.exists(out_basepth):
                Path(out_basepth).mkdir(parents=True, exist_ok=True)
                logger.info(f"created {out_basepth} directory")
            outfile = path.replace(".jpg", "_mask.png")
            cv2.imwrite(os.path.join(out_basepth, outfile), mask)
    if eval_on_train:
        logger.info("evaluating on train set")
        masks, flows, styles = model.eval(train_image_list, channels=[0, 0], diameter=my_diam_mean, cellprob_threshold=optimized_cellpose_threshold, flow_threshold=optimized_flow_treshhold)
        for image, mask, path in zip(train_image_list, masks, train_dataset.images):
            image = np.moveaxis(image, 0, -1)
            mask = mask.astype(np.uint8)
            visualize_instances(path, mask, "train_pred", root_dir)
            if not os.path.exists(os.path.join(outpath, "train", "predictions")):
                os.makedirs(os.path.join(outpath, "train", "predictions"))
            outfile = path.replace(".jpg", "_mask.png")
            cv2.imwrite(os.path.join(outpath, "train", "predictions", outfile), mask)
            

        logger.info(f" visualizing predictions on train images")

if __name__ == "__main__":
    #
    global outpath
    global active_learning
    global root_dir
    global test_root
    global evaluate
    global usegpu
    global epochs
    global model_path
    global logger
    global train
    global eval_on_train
    from cellpose.io import logger_setup

    logger = logging.getLogger(__name__)
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"

    handler = logging.StreamHandler(sys.stdout)
    logging.basicConfig(level=logging.INFO, format=log_fmt, handlers=[handler])

    parser = argparse.ArgumentParser()
    parser.add_argument("--outpath", help="output path", type=str)
    parser.add_argument(
        "--active-learning",
        help="active learning",
        default=False,
        action="store_true",
    )
    parser.add_argument("--root-dir", help="root directory of data", type=str)
    parser.add_argument(
        "--evaluate", help="evaluate", default=False, action="store_true"
    )
    parser.add_argument("--usegpu", help="use gpu", default=False, action="store_true")
    parser.add_argument("--epochs", help="number of epochs", default=1, type=int)
    parser.add_argument("--notrain", help="should the model be trained", default=True, action="store_false")
    parser.add_argument("--eval-on-train", help="evaluate on train set", default=False, action="store_true")
    args = parser.parse_args()
    outpath = args.outpath
    eval_on_train = args.eval_on_train
    logger_setup(cp_path=os.path.join('development', 'confluence', outpath))
    epochs = args.epochs
    active_learning = args.active_learning
    root_dir = args.root_dir
    test_root = os.path.join(root_dir, "test")
    evaluate = args.evaluate
    usegpu = args.usegpu
    logger.info(f'usegpu: {usegpu}')
    # is true when argument is not given, so training by default
    train = args.notrain
    main()
