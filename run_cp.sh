#!/bin/bash
#SBATCH --job-name=cellpose-test
#SBATCH --output=/home/sc.uni-leipzig.de/me792rqay/development/confluence/cp_normal_slurm_%a_%j.out
#SBATCH --error=/home/sc.uni-leipzig.de/me792rqay/development/confluence/cp_normal_slurm_%a_%j.err
#SBATCH --mail-type=ALL
#SBATCH --time=47:00:00
#SBATCH --gres=gpu:v100:1
#SBATCH --mem=511G
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --partition=clara
datapath=$1
outpath=$2
gtpath=$3
maskpath=$4
mask_ext=$5
train=$6

ml CUDA/11.7.0
ml cuDNN/8.4.1.50-CUDA-11.7.0

cd development/confluence
source confluence_env/bin/activate

echo 'check CUDA'
python3 -c 'import torch; from torch.utils.cpp_extension import CUDA_HOME; print(torch.cuda.is_available(), CUDA_HOME)'


if [ $train -eq 1 ]
then
    echo "training"
    python cellpose_main.py --active-learning --root-dir $datapath --evaluate --usegpu --epochs 500 --outpath $outpath
fi
python evaluate.py --gt-path=$gtpath --gt-ext=$mask_ext --pred-path=$maskpath --pred-ext=$mask_ext --run-id=$outpath

