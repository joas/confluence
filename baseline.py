"""
skimage.measure.find_contours(image, level=None, fully_connected='low', positive_orientation='low', *, mask=None)[source]
Find iso-valued contours in a 2D array for a given level value.
https://scikit-image.org/docs/stable/api/skimage.measure.html#skimage.measure.find_contours
Uses the “marching squares” method to compute a the iso-valued contours of the input 2D array for a particular level value. Array values are linearly interpolated to provide better precision for the output contours.
"""

import glob
from skimage import draw
from skimage import morphology
from skimage.feature import canny
from scipy import ndimage as ndi
import os
import skimage.io as io
from skimage import measure
from skimage.segmentation import clear_border, flood, flood_fill
import cv2
import skimage
from skimage.color import rgb2gray
import numpy as np
import subprocess
import matplotlib.pyplot as plt


def close_contour(contour):
    if not np.array_equal(contour[0], contour[-1]):
        contour = np.vstack((contour, contour[0]))
    return contour
def save_mask(id_key, path, mask):
    fullpath = os.path.join(id_key, "preds")
    if not os.path.exists(fullpath):
        os.makedirs(fullpath, exist_ok=True)
    path = path.replace(".jpg","_mask.png")
    io.imsave(os.path.join(fullpath, path), mask)
    
def baseline(id_key, images):
    masks = []
    paths = images[1]
    images = images[0]

    for image, path in zip(images, paths):
        
        edges = canny(image)
        filled = ndi.binary_fill_holes(input=edges)
        image = morphology.remove_small_objects(filled, 10)
 
        contours = measure.find_contours(image, 0.5)
        closed_contours = [measure.approximate_polygon(close_contour(contour), tolerance=0) for contour in contours]
        # draw the contours on the image
        mask = np.zeros_like(image, dtype=np.uint8)
        for contour in closed_contours:
            contour = np.round(contour).astype(int)
            rr, cc = draw.polygon(contour[:, 0], contour[:, 1])
            mask[rr, cc] = 255
        # use neares interpolation to fill the black pixels between the white masks also white
        mask_interpolated = skimage.transform.resize(mask, (image.shape[0], image.shape[1]), order=0, mode='constant', anti_aliasing=False)
        masks.append(mask_interpolated)
        save_mask(id_key, os.path.basename(path), mask)
 
    return masks


if __name__ == "__main__":
    data_dict = {
    "base-sc": "data/standardmicro/SC_GENERALPREP_data/test/",
    "base-lc-internal": "data/livecell/LC_GENERALPREP/04cur_data/test/",
    "base-lc-internallazy": "data/livecell/LC_GENERALPREP_LAZY/04cur_data/test/",
    "base-lc-external": "data/livecell_external/SUBSET/test/"
    }

    arr_dict = {
        "base-sc": "",
        "base-lc-internal": "",
        "base-lc-internallazy": "",
        "base-lc-external": ""
    }


    gt_dict = {
        "base-sc": "data/standardmicro/Instance_prep/SC_GENERALPREP_data/test/masks/",
        "base-lc-internal": "data/livecell/Instance_prep/LC_GENERALPREP_data/test/masks/",
        "base-lc-internallazy": "data/livecell/Instance_prep/LC_GENERALPREP_LAZY_data/test/masks/",
        "base-lc-external": "data/livecell_external/SUBSET/Instance_prep/test/masks/"
    }
    # READ AND PREPROCESS ----------------------------------------------
    for k,v in data_dict.items():
        print(k)
        cur_imgs = glob.glob(f"{v}*.jpg")
        img_arrays = [io.imread(path) for path in cur_imgs]
        if len(img_arrays[0].shape) == 3:
            img_arrays = [rgb2gray(img) for img in img_arrays]

        print(img_arrays[0].shape)
        print(f'len of img array: {len(img_arrays)}')
        arr_dict[k] = [img_arrays, cur_imgs]
        print(len(cur_imgs))
    # BASELINE PREDICTIONS ----------------------------------
    pred_dict = {}
    for k,v in arr_dict.items():
        print(k)
        pred_dict[k] = baseline(k,v)
        
    # EVAL SCRIPT ------------------------------------------
    

    for k,v in gt_dict.items():
        print(k)
        pred_path = os.path.join(k, "preds")
        call = ["python", 
                "evaluate.py",
                f"--gt-path={v}",
                "--gt-ext=png",
                f"--pred-path={pred_path}",
                "--pred-ext=png",
                f"--run-id={k}"
               ]
        print(call)
        subprocess.run(call)