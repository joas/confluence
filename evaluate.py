import numpy as np
import argparse
import logging
import sys
import os
import pandas as pd

import glob
import cv2
from PIL import Image


def get_centroid(mask):
    moments = cv2.moments(mask)

    # Check if the area (m00) is zero
    if moments["m00"] == 0:
        return None

    cx = int(moments["m10"] / moments["m00"])
    cy = int(moments["m01"] / moments["m00"])
    return cx, cy


def calc_mask_wise_io(
    mask_image1,
    mask_image2,
    mask_coordinates1,
    mask_coordinates2,
    mask_centroids1,
    mask_centroids2,
):
    # Calculate IoU for each pair of masks with the most similar position
    ious = []
    for i, (coordinates1, centroid1) in enumerate(
        zip(mask_coordinates1, mask_centroids1)
    ):
        mask1 = np.zeros_like(mask_image1, dtype=np.uint8)
        coordinates1 = np.array(coordinates1, dtype=np.int32)  # Convert coordinates to numpy array
        coordinates1 = coordinates1.reshape((-1, 1, 2))  # Reshape coordinates to (n, 1, 2) shape
        cv2.fillPoly(mask1, coordinates1, 1)

        min_distance = float("inf")
        best_j = -1

        for j, (coordinates2, centroid2) in enumerate(
            zip(mask_coordinates2, mask_centroids2)
        ):
            mask2 = np.zeros_like(mask_image2, dtype=np.uint8)
            coordinates2 = np.array(coordinates2, dtype=np.int32)  # Convert coordinates to numpy array
            coordinates2 = coordinates2.reshape((-1, 1, 2))  # Reshape coordinates to (n, 1, 2) shape
            cv2.fillPoly(mask2, np.array(coordinates2), 1)

            # Calculate centroids
            centroid1 = get_centroid(mask1)
            centroid2 = get_centroid(mask2)

            # Check if either centroid is None (due to zero area)
            if centroid1 is None or centroid2 is None:
                continue

            # Calculate Euclidean distance between centroids
            distance = np.sqrt(
                (centroid1[0] - centroid2[0]) ** 2 + (centroid1[1] - centroid2[1]) ** 2
            )

            # Update if this pair has a smaller distance
            if distance < min_distance:
                min_distance = distance
                best_j = j

        if best_j != -1:
            # Get the common region between two masks
            mask2 = np.zeros_like(mask_image2, dtype=np.uint8)
            coordinates2 = np.array(mask_coordinates2[best_j], dtype=np.int32)  # Convert coordinates to numpy array
            coordinates2 = coordinates2.reshape((-1, 1, 2))  # Reshape coordinates to (n, 1, 2) shape
            cv2.fillPoly(mask2, coordinates2, 1)

            # Calculate IoU
            u,i = get_iou(mask1, mask2)
            ious.append(i/(u+1e-6))
            # logger.info(f"IoU between Mask {i + 1} and Mask {best_j + 1}: {iou}")
        else:
            logger.info(f"No matching mask found for Mask {i + 1}")
    return ious


def get_iou(gt_mask, pred_mask):
    """Compute the intersection sum and union sum of two masks
    Args:
        gt_mask: ground truth mask
        pred_mask: predicted mask
    Returns:
        union_sum: sum of union of two masks
        intersection_sum: sum of intersection of two masks

    """
    intersection = np.logical_and(gt_mask, pred_mask)
    union = np.logical_or(gt_mask, pred_mask)
    union_sum = np.sum(union)
    intersection_sum = np.sum(intersection)

    return union_sum, intersection_sum


def get_metrics(gt_mask, pred_mask):
    """Compute the metrics of two masks, including true positive, false positive, false negative, precision, recall, f1
    Args:
        gt_mask: ground truth mask
        pred_mask: predicted mask
    Returns:
        true_positive: true positive
        false_positive: false positive
        false_negative: false negative
        f1: f1 score
        precision: precision
        recall: recall

    """
    # a true positive is when the pixelx value is 255 and the gt is 255
    # a false positive is when the pixel value is 255 but the gt is 0
    # a false negative is when the pixel value is 0 but the gt is 255
    # a true negative is when the pixel value is 0 and the gt is 0
    true_positive = np.sum(np.logical_and(gt_mask == 255, pred_mask == 255))
    false_positive = np.sum(np.logical_and(gt_mask == 0, pred_mask == 255))
    false_negative = np.sum(np.logical_and(gt_mask == 255, pred_mask == 0))
    true_negative = np.sum(np.logical_and(gt_mask == 0, pred_mask == 0))

    # true_negative = np.sum(np.logical_and(gt_mask == 0, pred_mask == 0))
    precision = true_positive / (true_positive + false_positive + 1e-6)
    recall = true_positive / (true_positive + false_negative + 1e-6)
    f1 = 2 * (precision * recall) / (precision + recall + 1e-6)
    accuracy = (true_positive + true_negative) / (
        true_positive + true_negative + false_positive + false_negative + 1e-6
    )
    logger.info(f"Current metrics: precision: {precision}, recall: {recall}, f1: {f1}")
    return (
        true_positive,
        false_positive,
        false_negative,
        f1,
        precision,
        recall,
        true_negative,
        accuracy,
    )


def same_shape(gt_mask, pred_mask):
    """Check if two masks are of the same shape
    Args:
        gt_mask: ground truth mask
        pred_mask: predicted mask
    Returns:
        gt_mask: ground truth mask with the same shape as pred_mask
        pred_mask: predicted mask with the same shape as gt_mask

    """
    arr1 = fix_pixel_value(np.array(gt_mask))
    arr2 = fix_pixel_value(np.array(pred_mask))
    # check if shape is reversed
    if arr1.shape != arr2.shape:
        if arr1.shape == arr2.T.shape:
            arr2 = arr2.T
        else:
            raise ValueError(
                f"Masks are not of the same shape: {arr1.shape} (gt_mask), {arr2.shape}"
            )
    return arr1, arr2




def fix_pixel_value(mask):
    """replaces non zero pixel values with 255
    Args:
        mask (np.array): mask to be fixed
    Returns:
        mask (np.array): fixed mask

    """
    mask[mask > 0] = 255
    return mask


def get_total_metrics(gt_paths, gt_ext, pred_paths, pred_ext):
    """Compute the total metrics of for all masks in the folder
    Args:
        gt_paths: list of paths to ground truth masks
        gt_ext: extension of ground truth masks
        pred_paths: list of paths to predicted masks
        pred_ext: extension of predicted masks
    Returns:
        res_dict: dictionary of metrics

    """
    res_dict = {
        "images": [],
        "gt_confluence": [],
        "iou_mask_wise": [],
        "pred_confluence": [],
        "relative_delta_confluence": [],
        "iou": [],
        "precision": [],
        "recall": [],
        "f1": [],
        "true_negative": [],
        "accuracy": [],
    }
    union_sum = 0
    intersection_sum = 0
    tp_sum = 0
    fp_sum = 0
    fn_sum = 0
    tn_sum = 0
    logger.info(pred_paths)
    for gt_path in gt_paths:
        logger.info("__________________________________\n")
        logger.info("NEW GET_PATH ---------------------------")
        logger.info(f"gt_path: {gt_path}")
        gt_path_match = os.path.basename(gt_path.replace(gt_ext, ""))
        logger.info(f"gt_path match: {gt_path_match}")

        for pred_path in pred_paths:
            # remove extension
            pred_path_match = os.path.basename(pred_path.replace(pred_ext, ""))
            pred_path_match = pred_path_match.replace("_segmentation", "")
            pred_path_match = pred_path_match.replace("_pred", "")
            logger.info(f"pred path {pred_path_match}")

            if pred_path_match == gt_path_match:
                logger.info("match")
                logger.info(f"gt_path: {gt_path}")
                gt_mask = Image.open(gt_path).convert("L")
                pred_mask = Image.open(pred_path).convert("L")
                contours1, _ = cv2.findContours(
                    np.array(gt_mask), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE
                )
                contours2, _ = cv2.findContours(
                    np.array(pred_mask), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE
                )
                mask_coords1 = [
                    contour.squeeze().astype(np.int32).tolist() for contour in contours1
                ]
                mask_coords2 = [
                    contour.squeeze().astype(np.int32).tolist() for contour in contours2
                ]
                mask_centroids1 = [
                    get_centroid(np.zeros_like(gt_mask, dtype=np.uint8))
                    for _ in contours1
                ]
                mask_centroids2 = [
                    get_centroid(np.zeros_like(pred_mask, dtype=np.uint8))
                    for _ in contours2
                ]
                gt_mask, pred_mask = same_shape(gt_mask, pred_mask)
                iou_mask_wise = calc_mask_wise_io(
                    gt_mask,
                    pred_mask,
                    mask_coords1,
                    mask_coords2,
                    mask_centroids1,
                    mask_centroids2,
                )

                # get intersection over union (IoU) of two masks
                union, intersection = get_iou(gt_mask, pred_mask)
                cur_iou = intersection / (union + 1e-6)

                tp, fp, fn, f1, prec, recall, tn, acc = get_metrics(gt_mask, pred_mask)

                confluence = np.sum(pred_mask == 255) / pred_mask.size + 1e-6
                conflunece_gt = np.sum(gt_mask == 255) / (gt_mask.size + 1e-6)
                relative_delta_confluence = (
                    np.abs(confluence - conflunece_gt) / conflunece_gt
                )
                res_dict["relative_delta_confluence"].append(relative_delta_confluence)
                res_dict["iou_mask_wise"].append(iou_mask_wise)
                res_dict["gt_confluence"].append(conflunece_gt)
                res_dict["pred_confluence"].append(confluence)
                res_dict["f1"].append(f1)
                res_dict["precision"].append(prec)
                res_dict["recall"].append(recall)
                res_dict["images"].append(gt_path_match)
                res_dict["iou"].append(cur_iou)
                res_dict["true_negative"].append(tn)
                res_dict["accuracy"].append(acc)
                union_sum += union
                intersection_sum += intersection
                tp_sum += tp
                fp_sum += fp
                fn_sum += fn
                tn_sum += tn
    total_precision = tp_sum / (tp_sum + fp_sum + 1e-6)
    total_recall = tp_sum / (tp_sum + fn_sum + 1e-6)
    total_f1 = (
        2 * (total_precision * total_recall) / (total_precision + total_recall + 1e-6)
    )
    total_accuracy = (tp_sum + tn_sum) / (tp_sum + tn_sum + fp_sum + fn_sum + 1e-6)
    total_iou = intersection_sum / (union_sum + 1e-6)
    # logger.info("total iou: ", total_iou)
    # logger.info("total pixel f1: ", total_f1)
    # logger.info("total pixel accuracy: ", total_accuracy)
    res_dict["images"].append("total")
    res_dict["gt_confluence"].append(np.mean(res_dict["gt_confluence"]))
    res_dict["pred_confluence"].append(np.mean(res_dict["pred_confluence"]))
    res_dict["precision"].append(total_precision)
    res_dict["recall"].append(total_recall)
    res_dict["f1"].append(total_f1)
    res_dict["iou"].append(total_iou)
    res_dict["true_negative"].append(tn_sum)
    res_dict["accuracy"].append(total_accuracy)
    res_dict["relative_delta_confluence"].append(np.mean(res_dict["relative_delta_confluence"]))
    # aggreate the iou_mask_wise metric"
    res_dict["iou_mask_wise"].append('iou mask wise')
    return res_dict


if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    # log stream handler to log to stdout
    handler = logging.StreamHandler(sys.stdout)
    logging.basicConfig(level=logging.INFO, format=log_fmt, handlers=[handler])

    # use argparse to parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--gt-path", dest="gt_paths", type=str, help="path to ground truth masks"
    )
    parser.add_argument(
        "--gt-ext", dest="gt_ext", type=str, help="extension of ground truth masks"
    )
    parser.add_argument(
        "--pred-path", dest="pred_paths", type=str, help="path to predicted masks"
    )
    parser.add_argument(
        "--pred-ext", dest="pred_ext", type=str, help="extension of predicted masks"
    )
    parser.add_argument("--run-id", dest="run_id", type=str, help="run id")
    parser.add_argument("--run-on-training", dest="run_on_training", type=bool, default=False, help="run on training data or not")
    args = parser.parse_args()

    gt_paths = args.gt_paths
    gt_ext = args.gt_ext
    pred_paths = args.pred_paths
    pred_ext = args.pred_ext
    run_id = args.run_id
    gt_paths = glob.glob(gt_paths + "/*." + gt_ext)
    
    print(f"GT PATHS: {gt_paths}")
    pred_paths = glob.glob(pred_paths + "/*." + pred_ext)
    res_dict = get_total_metrics(gt_paths, gt_ext, pred_paths, pred_ext)
    for k, v in res_dict.items():
        logger.info(f"{k}: {len(v)}")
    res_df = pd.DataFrame(res_dict)
    eval_file = "eval_metrics.csv" if not args.run_on_training else "eval_metrics_train.csv"
    outpath = os.path.join(run_id, eval_file)
    res_df.to_csv(outpath, index=False)
