#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --time=7-00:00:00
#SBATCH --gres=gpu:1
#SBATCH --mem=256G
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
cd 'development/confluence'

ml GCCcore/10.2.0
ml CUDA/11.7.0
ml Python/3.8.6


source confluence_env/bin/activate

python al_cp.py "--configfile="$1 "--metarun="$2 "--num-exp=1" "--randomrun="$3 "--para" $4
echo "finisshed script"
# get name of %1 without extension
name=$(basename $1 .toml)
echo $name
echo "finished job"
