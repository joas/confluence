import os
import sys
import pycocotools.coco as COCO


if __name__ == '__main__':
    in_paths = sys.argv[1]
    outpath_train = sys.argv[2]
    outpath_test = sys.argv[3]
    train_path = os.path.join(in_paths, 'train')
    test_path = os.path.join(in_paths, 'test')

    coco_test = COCO.COCO(os.path.join(test_path, 'annotations.json'))
    coco_train = COCO.COCO(os.path.join(train_path, 'annotations.json'))
    # clean coco file so that all annoations have an area > 80
   

    # create cur data dir
    os.makedirs('./data/04cur_data', exist_ok=True)
    os.makedirs(os.path.join(outpath_train), exist_ok=True)
    os.makedirs(os.path.join(outpath_test), exist_ok=True)


    # move train images
    for k in coco_train.imgs.keys():
        img_name = coco_train.imgs[k]['file_name']
        img_path = os.path.join(in_paths, img_name)
        os.system('cp {} {}'.format(img_path, os.path.join(outpath_train)))
        # check if unet case
        mask_check_path = outpath_train.replace('imgs', 'masks')
        if 'imgs' in outpath_train:
            os.makedirs(mask_check_path, exist_ok=True)
            mask_name = img_name.split('.')[0] + '_mask.png'
            mask_path = os.path.join(in_paths, 'masks', mask_name)
            os.system('cp {} {}'.format(mask_path, os.path.join(mask_check_path)))
            continue # we should not copy the annotation file for unet case
        # cp annotation
        ann_path = os.path.join(in_paths, 'train', 'annotations.json')
        os.system('cp {} {}'.format(ann_path, os.path.join(outpath_train)))



    # move test images
    for k in coco_test.imgs.keys():
        img_name = coco_test.imgs[k]['file_name']
        img_path = os.path.join(in_paths, img_name)
        os.system('cp {} {}'.format(img_path, os.path.join(outpath_test)))
        # check if unet case
        mask_check_path = outpath_test.replace('imgs', 'masks')
        if 'imgs' in outpath_test:
            os.makedirs(mask_check_path, exist_ok=True)
            mask_name = img_name.split('.')[0] + '_mask.png'
            mask_path = os.path.join(in_paths, 'masks', mask_name)
            os.system('cp {} {}'.format(mask_path, os.path.join(mask_check_path)))
            continue # we should not copy the annotation file for unet case
        # cp annotation
        ann_path = os.path.join(in_paths, 'test', 'annotations.json')
        os.system('cp {} {}'.format(ann_path, os.path.join(outpath_test)))

