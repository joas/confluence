""" This script takes a list of masks and original imiages and creates an overlay"""


import os
import cv2
import numpy as np
import glob
import sys
def overlay(image, mask, color, alpha, resize=None):
    """Combines image and its segmentation mask into a single image.
    https://www.kaggle.com/code/purplejester/showing-samples-with-segmentation-mask-overlay

    Params:
        image: Training image. np.ndarray,
        mask: Segmentation mask. np.ndarray,
        color: Color for segmentation mask rendering.  tuple[int, int, int] = (255, 0, 0)
        alpha: Segmentation mask's transparency. float = 0.5,
        resize: If provided, both image and its mask are resized before blending them together.
        tuple[int, int] = (1024, 1024))

    Returns:
        image_combined: The combined image. np.ndarray

    """
    color = color[::-1]
    colored_mask = np.expand_dims(mask, 0).repeat(3, axis=0)
    colored_mask = np.moveaxis(colored_mask, 0, -1)
    masked = np.ma.MaskedArray(image, mask=colored_mask, fill_value=color)
    image_overlay = masked.filled()

    if resize is not None:
        image = cv2.resize(image.transpose(1, 2, 0), resize)
        image_overlay = cv2.resize(image_overlay.transpose(1, 2, 0), resize)

    image_combined = cv2.addWeighted(image, 1 - alpha, image_overlay, alpha, 0)

    return image_combined

if __name__ == '__main__':
    mask_path = sys.argv[1]
    mask_ext = sys.argv[2]
    img_path = sys.argv[3]
    img_ext = sys.argv[4]
    masks = glob.glob(os.path.join(mask_path, f'*.{mask_ext}'))
    imgs = glob.glob(os.path.join(img_path, f'*.{img_ext}'))
    for mask in masks:
        mask_name = os.path.basename(mask)
        mask_name = mask_name.replace(f'.{mask_ext}', '')
        mask_name = mask_name.replace('_mask', '')
        # print(f'mask name: {mask_name}')
        for img in imgs:
            img_name = os.path.basename(img)
            img_name = img_name.replace(f'.{img_ext}', '')
            # print(f'img name: {img_name}')
            if mask_name == img_name:
                print(f'processing {img_name}')
                mask = cv2.imread(mask)
                # mask to grayscale
                mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
                print(f'mask shape: {mask.shape}')
                img = cv2.imread(img)
                print(f'img shape: {img.shape}')
                # break
                image_with_pred = overlay(img, mask, color=(255,0,0), alpha=0.41)
                image_with_pred = image_with_pred * 255
                image_with_pred = image_with_pred.astype(np.uint8)
                outpath = os.path.join(mask_path, f'{img_name}_overlay.png')
                print(f' saving to {outpath}')
                cv2.imwrite(outpath, image_with_pred)
