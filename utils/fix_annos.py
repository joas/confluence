import sys
import json
def fix_annos(path):

    """If annotations have 4 points they get confused with bounding boxes.
    This function adds a fake point to the annotation to make it a polygon.
    ARGS:
        path: path to json file
    RETURNS:
        None

    """
     # fix annotations:
    with open(path, 'r') as f:
        json_obj = json.load(f)
    for i, instance in enumerate(json_obj['annotations']):

        if len(instance['segmentation'][0]) <= 4:
            print(f' insance {i} has less than 4 points {instance["segmentation"][0]}')
            print(f'Adding fake points to instance {i}')
            fake_x = instance['segmentation'][0][0] + 1
            fake_y = instance['segmentation'][0][1] + 1
            instance['segmentation'][0].append(fake_x)
            instance['segmentation'][0].append(fake_y)
            print(f'new instance {i} has {instance["segmentation"][0]}')
            # remove segmentation with area < 80
        if instance['area'] < 50:
            print(f'popping instance {i} with area {instance["area"]}')
            json_obj['annotations'].pop(i)
                
    # save fixed annotations
    with open(path, 'w') as f:
        json.dump(json_obj, f)

if __name__ == '__main__':
    path = sys.argv[1]
    fix_annos(path)