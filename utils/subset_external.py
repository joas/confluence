"""
data structure:
    BASEPATH
    ---pool
    ---train
    ---test
now we want to create a new parent dir SUBSET. In this dir we want to create a new dir for each pool, train, test. In these theres will be the images that
we selected for the subset. We select the subset based on the annotationjson
files. In the corrsponding direcotries. Next we move the selected images and
create a new annotationjson file for the subset.
Lastly, we have a directory <BASEPATH>/Instance_prep/LC_EXT_GENERALPREP_data"
with the following structure:
    ---imgs
    ---masks
    ---annotations
    ---pool
       ---imgs
       ---masks
    ---test
        ---imgs
        ---masks

We want to create a new directory <BASEPATH>/Instance_prep/LC_EXT_GENERALPREP_data/SUBSET
with the same structure as above, but with the same subset as above.

"""
import sys
import os
import json
import shutil
import random
from pycocotools.coco import COCO

global BASEPATH
global SUBSETPATH
global SUBSETSIZE
global SUBSETNAME

SUBSETNAME = "SUBSET"
CELLLINE = "random"
SUBSETSIZE = 100
BASEPATH = os.path.join("data/livecell_external")
SUBSETPATH = os.path.join(BASEPATH, "SUBSET")
INSTANCEPATH = os.path.join(SUBSETPATH, "Instance_prep")


def create_subset_dir():
    if not os.path.exists(SUBSETPATH):
        print(f"created subset dir {SUBSETPATH}")
        os.makedirs(SUBSETPATH, exist_ok=True)
    if not os.path.exists(INSTANCEPATH):
        print(f"created subset dir {INSTANCEPATH}")
        os.makedirs(INSTANCEPATH, exist_ok=True)


def create_subset_dir_structure():
    subset_structure = ["train", "annotations", "pool", "test"]
    for sub in subset_structure:
        sub_path = os.path.join(SUBSETPATH, sub)
        if not os.path.exists(sub_path):
            print(f"created subset dir {sub_path}")
            os.makedirs(sub_path)
    instance_structs = [
        "imgs",
        "masks",
        "annotations",
        "pool/imgs",
        "pool/masks",
        "test/imgs",
        "test/masks",
    ]
    for sub in instance_structs:
        sub_path = os.path.join(INSTANCEPATH, sub)
        if not os.path.exists(sub_path):
            print(f"created subset dir {sub_path}")
            os.makedirs(sub_path, exist_ok=True)


def create_subset_annotationjson(flag):
    actual_files = os.listdir(os.path.join(BASEPATH, flag))
    actual_files = [os.path.basename(f) for f in actual_files]
    print(f"number files in {flag} are len {len(actual_files)}")
    # remove the annotationjson file

    helper = {"pool": "train", "test": "test"}
    annofile_name = "master.json" if flag == "pool" else "annotations.json"
    annotationjson_path = os.path.join(BASEPATH, helper[flag], annofile_name)
    print(f"annotationjson_path is {annotationjson_path} for flag {flag}")
    coco = COCO(annotationjson_path)
    img_ids = coco.getImgIds()
    print(f"number of images in {flag} is {len(img_ids)}")
    subsize = SUBSETSIZE if flag == "pool" else int(SUBSETSIZE / 5)
    print(f"subset size for {flag} is {subsize}")
    if CELLLINE == "random":
        subset_ids = random.sample(img_ids, subsize)
        # remove ids if file name is not in actual files
        subset_ids = [iid for iid in subset_ids if coco.imgs[iid]["file_name"] in actual_files]
    else:
        subset_ids = []
        for iid in img_ids:
            file_name = coco.imgs[iid]["file_name"]
            if CELLLINE in file_name and file_name in actual_files:
                subset_ids.append(iid)
    print(f" first 5 ids of {flag} are {subset_ids[:5]} len {len(subset_ids)}")
    keep_filenames = []
    for iid in subset_ids:
        file_name = coco.imgs[iid]["file_name"]
        if file_name in actual_files:
            keep_filenames.append(coco.imgs[iid]["file_name"])
    with open(annotationjson_path, "r") as f:
        data = json.load(f)
    data["annotations"] = [
        anns for anns in data["annotations"] if anns["image_id"] in subset_ids
    ]
    data["images"] = [
        imgs for imgs in data["images"] if imgs["id"] in subset_ids
    ]
    # save subset annotationjson
    if flag == "test":
        subset_annotationjson_path = os.path.join(SUBSETPATH, flag, annofile_name)
        print(
            f"dumping subset annotationjson to {subset_annotationjson_path} for flag {flag}"
        )
        with open(subset_annotationjson_path, "w") as f:
            json.dump(data, f)

    if flag == "pool":
        pool_file = os.path.join(SUBSETPATH, "pool", "annotations.json")
        master_file = os.path.join(SUBSETPATH, "train", "master.json")
        print(f"dumping subset annotationjson to {pool_file} for flag {flag}")
        with open(pool_file, "w") as f:
            json.dump(data, f)
        print(f"dumping subset annotationjson to {master_file} for flag {flag}")
        with open(master_file, "w") as f:
            json.dump(data, f)
    return keep_filenames


def move_from_train_to_pool():
    """make sure that all images are in the pool, so that we can move them
    to the subset and that the structure is correcrt  for the active learning
    later, since we need all images in the pool, as wel las the annotations.json
    only the master.json should be in trainp

    """
    train_files = os.listdir(os.path.join(BASEPATH, "train"))
    print(f"number of files in train is {len(train_files)}")
    for file in train_files:
        if file == "master.json":
            print(f"skipped {file}")
            continue
        shutil.move(
            os.path.join(BASEPATH, "train", file), os.path.join(BASEPATH, "pool", file)
        )
    instance_in_path = os.path.join(
        BASEPATH, "Instance_prep", "LC_EXT_GENERALPREP_data"
    )
    mask_files = os.listdir(os.path.join(instance_in_path, "masks"))
    print(f"number of files in masks for Instance_prep is {len(mask_files)}")
    for file in mask_files:
        shutil.move(
            os.path.join(instance_in_path, "masks", file), INSTANCEPATH, "pool", "masks"
        )
    img_files = os.listdir(os.path.join(instance_in_path, "imgs"))
    print(f"number of files in imgs for Instance_prep is {len(img_files)}")
    for file in img_files:
        shutil.move(
            os.path.join(instance_in_path, "imgs", file), INSTANCEPATH, "pool", "imgs"
        )

def filter_annotations_for_not_found(not_found, flag):
    """ NOT USED"""
    coco_file = os.path.join(SUBSETPATH, flag, 'annotations.json')
    if flag == 'test':
        coco = COCO(coco_file)
        filenames = [coco.imgs[iid]['file_name'] for iid in coco.getImgIds()]
        keep_filenames = [fn for fn in filenames if fn not in not_found]
        keep_ids = [iid for iid in coco.getImgIds() if coco.imgs[iid]['file_name'] in keep_filenames]
        with open(coco_file, 'r') as f:
            data = json.load(f)
        # TODO use data not subset_coco
        subset_coco = {}
        subset_coco['annotations'] = [anns for anns in data['annotations'] if anns['image_id'] in keep_ids]
        subset_coco['images'] = [imgs for imgs in data['images'] if imgs['id'] in keep_ids]
        subset_coco['categories'] = data['categories']
        print(f'dumping subset annotationjson to {coco_file} for flag {flag}')
        print(f'file contains {len(subset_coco["images"])} images')
        print(f' file contains {len(subset_coco["annotations"])} annotations')
        with open(coco_file, 'w') as f:
            json.dump(subset_coco, f)
        return
    if flag == 'pool':

        master_file = os.path.join(SUBSETPATH, 'train', 'master.json')
        coco = COCO(master_file)
        filenames = [coco.imgs[iid]['file_name'] for iid in coco.getImgIds()]
        keep_filenames = [fn for fn in filenames if fn not in not_found]
        keep_ids = [iid for iid in coco.getImgIds() if coco.imgs[iid]['file_name'] in keep_filenames]
        with open(coco_file, 'r') as f:
            data = json.load(f)
        subset_coco = {}
        subset_coco['annotations'] = [anns for anns in data['annotations'] if anns['image_id'] in keep_ids]
        subset_coco['images'] = [imgs for imgs in data['images'] if imgs['id'] in keep_ids]
        subset_coco['categories'] = data['categories']
        print(f'dumping subset annotationjson to {coco_file} for flag {flag}')
        print(f'file contains {len(subset_coco["images"])} images')
        print(f' file contains {len(subset_coco["annotations"])} annotations')
        with open(coco_file, 'w') as f:
            json.dump(subset_coco, f)
        with open(master_file, 'w') as f:
            json.dump(subset_coco, f)
        return

def move_images_and_annotationjson(keep_filenames, flag):
    # move all selected images from BASEPATH/pool to BASEPATH/subset/pool
    instance_flag = "" if flag == "pool" else "/test"
    not_found = []
    for file in keep_filenames:
        try:
            shutil.move(
                os.path.join(BASEPATH, flag, file), os.path.join(SUBSETPATH, flag, file)
            )
        except Exception as e:
            print(f"file {file} not found in {BASEPATH}/{flag}")
            not_found.append(os.path.basename(file))
            print(e)
            continue
        # move annotationjson from BASEPATH/pool to BASEPATH/subset/pool
        maskfile = file.replace(".jpg", "_mask.png")
        try:
            shutil.move(
                os.path.join(
                    BASEPATH,
                    "Instance_prep",
                    "LC_EXT_GENERALPREP_data",
                    flag,
                    "masks",
                    maskfile,
                ),
                os.path.join(
                    INSTANCEPATH,
                    flag,
                    "masks",
                    maskfile,
                ),
            )
        except Exception as e:
            print(
                f"file {maskfile} not found in {BASEPATH}/Instance_prep/LC_EXT_GENERALPREP_data/{flag}/masks"
            )
            print(e)
            not_found.append(os.path.basename(maskfile))
            continue
        try:
            shutil.move(
                os.path.join(
                    BASEPATH,
                    "Instance_prep",
                    "LC_EXT_GENERALPREP_data",
                    flag,
                    "imgs",
                    file,
                ),
                os.path.join(
                    INSTANCEPATH,
                    flag,
                    "imgs",
                    file,
                ),
            )
        except Exception as e:
            print(
                f"file {file} not found in {BASEPATH}/Instance_prep/LC_EXT_GENERALPREP_data/{flag}/imgs"
            )
            print(e)
            not_found.append(os.path.basename(file))
            continue
    print(f"files not found are {not_found}")
    # get unique filenames
    # remove masks from not found
    not_found = [file.replace("_mask.png", ".jpg") for file in not_found]
    not_found = list(set(not_found))
    # save not found files to txx file
    with open("not_found.txt", "w") as f:
        for file in not_found:
            f.write(file + "\n")
    # remove not found files from annotationjson
    print(f"removing {len(not_found)} files from annotationjson")
    # filter_annotations_for_not_found(not_found, flag)



def main():
    print("Starting main")
    create_subset_dir()
    print("Created subset dir")
    create_subset_dir_structure()
    print("Created subset dir structure")
    move_from_train_to_pool()
    print("Moved files from train to pool")
    test_filenames = create_subset_annotationjson("test")
    print(
        "Created keep filenames for test first filename is: ",
        test_filenames[0],
        "and the length is ",
        len(test_filenames),
    )
    train_filenames = create_subset_annotationjson("pool")
    print(
        "Created keep filenames for train first filename is: ",
        train_filenames[0],
        "and the length is ",
        len(train_filenames),
    )
    move_images_and_annotationjson(test_filenames, "test")
    print("Moved test images and annotationjson")
    move_images_and_annotationjson(train_filenames, "pool")
    print("Moved train images and annotationjson")
    print("check if master.json is in train")
    if os.path.exists(os.path.join(SUBSETPATH, "pool", "master.json")):
        print("master.json is not in train, moving it")
        shutil.move(
            os.path.join(SUBSETPATH, "pool", "master.json"),
            os.path.join(SUBSETPATH, "train", "master.json"),
        )
    print("Finished main")


if __name__ == "__main__":
    main()
