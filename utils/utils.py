from detectron2.utils.visualizer import Visualizer
import random
from skimage import measure
import numpy as np
import os
import logging
import matplotlib.pyplot as plt
import cv2
from detectron2.utils.visualizer import ColorMode
from PIL import Image, ImageDraw
from detectron2.config import get_cfg
from detectron2 import model_zoo
import torch
import json
import os
global logger
logger = logging.getLogger(__name__)

def fix_annos(path):
    """If annotations have 4 points they get confused with bounding boxes.
    This function adds a fake point to the annotation to make it a polygon.
    ARGS:
        path: path to json file
    RETURNS:
        None

    """
     # fix annotations:
    with open(path, 'r') as f:
        json_obj = json.load(f)
    for i, instance in enumerate(json_obj['annotations']):
        if len(instance['segmentation'][0]) <= 4:
            print(f' insance {i} has less than 4 points {instance["segmentation"][0]}')
            print(f'Adding fake points to instance {i}')
            fake_x = instance['segmentation'][0][0] + 1
            fake_y = instance['segmentation'][0][1] + 1
            instance['segmentation'][0].append(fake_x)
            instance['segmentation'][0].append(fake_y)
            print(f'new instance {i} has {instance["segmentation"][0]}')
    # save fixed annotations
    with open(path, 'w') as f:
        json.dump(json_obj, f)

def setup_cfg(output_dir, iter=10):
    cfg = get_cfg()
    # config_name = "COCO-Detection/faster_rcnn_R_50_FPN_3x.yaml"
    config_name = "COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"
    cfg.merge_from_file(model_zoo.get_config_file(config_name))
    cfg.OUTPUT_DIR = output_dir
    cfg.DATALOADER.NUM_WORKERS = 1
    cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(config_name)
    cfg.SOLVER.IMS_PER_BATCH = 1
    if not torch.cuda.is_available():
        logger.info('cuda not available, using cpu')
        cfg.MODEL.DEVICE = 'cpu'
    cfg.SOLVER.BASE_LR = 0.00025
    cfg.SOLVER.WARMUP_ITERS = 5
    cfg.SOLVER.MAX_ITER = iter
    cfg.SOLVER.STEPS = []
    # Small value=Frequent save  need a lot of storage.
    cfg.SOLVER.CHECKPOINT_PERIOD = 100000
    cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 128
    cfg.MODEL.ROI_HEADS.NUM_CLASSES = 1
    cfg.SEED=100
    return cfg


def close_contour(contour):
    if not np.array_equal(contour[0], contour[-1]):
        contour = np.vstack((contour, contour[0]))
    return contour


def binary_mask_to_polygon(binary_mask, tolerance=0):
    """Converts a binary mask to COCO polygon representation
    Args:
    binary_mask: a 2D binary numpy array where '1's represent the object
    tolerance: Maximum distance from original points of polygon to approximated
    polygonal chain. If tolerance is 0, the original coordinate array is returned.
    """
    polygons = []
    # pad mask to close contours of shapes which start and end at an edge
    padded_binary_mask = np.pad(
        binary_mask, pad_width=1, mode="constant", constant_values=0
    )
    contours = measure.find_contours(padded_binary_mask, 0.5)
    # contours = np.subtract(contours, 1)
    for contour in contours:
        contour = close_contour(contour)
        contour = measure.approximate_polygon(contour, tolerance)
        if len(contour) < 3:
            continue
        contour = np.flip(contour, axis=1)
        segmentation = contour.ravel().tolist()
        # after padding and subtracting 1 we may get -0.5 points in our segmentation
        segmentation = [0 if i < 0 else i for i in segmentation]
        polygons.append(segmentation)

    return polygons


def get_confluence(data_dict, pred):
    """calcuclates impedance from detectron predictions

    ARGS:
        data_dict - (dict): metadata about image and annotations
        pred - (dict): data of detectron2 predictionsa
    RETURNS:
        confluence - (float): percentage of cells on image

    """

    masks = pred["instances"].pred_masks
    file_name = data_dict["file_name"]
    if not type(file_name) == str:
        file_name = file_name.name  # when file comes from webapp
    annotations = []
    pixel_sum = 0
    for i in range(len(pred["instances"])):
        mask_array = masks[i, :, :].detach().cpu().clone().numpy()
        pixel_sum += mask_array.sum()
        try:
            segmentation = binary_mask_to_polygon(mask_array)
            annotations.append(segmentation)
        except Exception as e:
            logger.warning(e)
            logger.warning(
                f"Could not create polygon from prediction {i} from image {file_name}"
            )
            logger.warning(f"Shape: {np.unique(mask_array)}")

    confluence = pixel_sum / (data_dict["height"] * data_dict["width"])
    return confluence, annotations


def visualize_result2(data_dict, anns, output_dir="output"):

    # img_out = Image.new("L", [data_dict["height"], data_dict["width"], ])
    img_out = Image.new("L", [data_dict["width"], data_dict["height"], ])
    print(f'img_out size: {img_out.size}')
    print(f' with form data: {data_dict["width"]} and {data_dict["height"]} ')

    file_name = data_dict["file_name"]
    logger.info(file_name)
    org_image = Image.open(file_name).convert("RGBA")

    img_out1 = ImageDraw.Draw(img_out)
    overlay_out = ImageDraw.Draw(org_image, "RGBA")
    print('with overlay')
    for i in range(len(anns)):
        if len(anns[i]) == 0:
            continue
        try:
            segs = anns[i][0]
            img_out1.polygon(segs, fill="white", outline="white")
            overlay_out.polygon(segs, fill=(
                21, 239, 116, 27), outline="#B7FF00")
        except Exception as e:
            logger.warning(e)
            logger.warning(
                f" Could not create mask for {i} for file: {file_name}")

    if not os.path.exists(os.path.join(output_dir, "masks")):
        logger.info("create output dir: ./masks")
        os.mkdir(os.path.join(output_dir, "masks"))
    if not os.path.exists(os.path.join(output_dir, "masks", "overlay")):
        os.mkdir(os.path.join(output_dir, "masks", "overlay"))

    # for case that file comes from webapp
    if not type(file_name) == str:
        file_name = file_name.name
    outfile = os.path.join(output_dir, "masks", os.path.basename(
        file_name).split(".")[0] + "_mask" + ".png")
    overlay_file = os.path.join(
        output_dir, "masks", "overlay", os.path.basename(file_name).split(".")[
            0] + "_overlay" + ".png"
    )
    org_image.save(overlay_file)
    img_out.save(outfile)
    return org_image


logger = logging.getLogger(__name__)


def plt_loss_curve(metrics_df, show=False, output_dir="output"):
    """Plot loss curve from metrics dataframe"""

    fig, ax = plt.subplots()
    clean_df = metrics_df[~metrics_df["total_loss"].isna()]
    ax.plot(clean_df["iteration"],
            clean_df["total_loss"], c="C0", label="train")
    if "validation_loss" in metrics_df.columns:
        metrics_df2 = metrics_df[~metrics_df["validation_loss"].isna()]
        ax.plot(
            metrics_df2["iteration"],
            metrics_df2["validation_loss"],
            c="C1",
            label="validation",
        )
    ax.legend()
    ax.set_title("Loss curve")
    ax.set_xlabel("Iteration")
    ax.set_ylabel("Loss")
    if show:
        plt.show()
    fig.savefig(os.path.join(output_dir, "loss_curve.png"))
    logger.info(f"Loss curve saved to {output_dir}/loss_curve.png")
    plt.close(fig)


def plt_acc_curve(metrics_df, show=False, output_dir="output"):
    fig, ax = plt.subplots()

    clean_df = metrics_df[~metrics_df["fast_rcnn/cls_accuracy"].isna()]
    ax.plot(
        clean_df["iteration"], clean_df["fast_rcnn/cls_accuracy"], c="C0", label="train"
    )
    # ax.set_ylim([0, 0.5])
    ax.legend()
    ax.set_title("cls_accuracy")
    ax.set_xlabel("iteration")
    ax.set_ylabel("accuracy")
    if show:
        plt.show()
    fig.savefig(os.path.join(output_dir, "cls_accuracy.png"))
    logger.info(f"cls_accuracy saved to {output_dir}/cls_accuracy.png")


def visualize_data(dataset_dicts, metadata_dicts, output_dir="output"):
    """Visualize sample training data
    Params:
        dataset_dicts: dictionary of dataset names and paths
        metadata_dicts: dictionary of dataset names and metadata
    Returns:
        None

    """
    fig, ax = plt.subplots(2, 2, figsize=(20, 14))
    indices = [ax[0][0], ax[1][0], ax[0][1], ax[1][1]]
    for i, d in enumerate(random.sample(dataset_dicts, 4)):
        img = cv2.imread(d["file_name"])
        visualizer = Visualizer(
            img[:, :, ::-1],
            metadata=metadata_dicts,
            scale=0.5,
            instance_mode=ColorMode.IMAGE_BW,
        )
        vis = visualizer.draw_dataset_dict(d)
        indices[i].grid(False)
        indices[i].axis("off")
        indices[i].imshow(vis.get_image()[:, :, ::-1])
        indices[i].set_title(d["file_name"].split("/")[-1])
    if not os.path.exists(output_dir):
        logger.info(f"no output dir found, creating {output_dir}")
        os.mkdir(output_dir)
    fig.savefig(os.path.join(output_dir, "sample_data.png"))
    logger.info(f"Sample data saved to {output_dir}/sample_data.png")
    plt.close(fig)

    if not os.path.exists(os.path.join(output_dir, "input_visualize")):
        os.mkdir(os.path.join(output_dir, "input_visualize"))
    for d in dataset_dicts:
        img = cv2.imread(d["file_name"])
        visualizer = Visualizer(
            img[:, :, ::-1],
            metadata=metadata_dicts,
            scale=0.5,
            instance_mode=ColorMode.IMAGE_BW,
        )
        vis = visualizer.draw_dataset_dict(d)
        plt.imshow(vis.get_image()[:, :, ::-1])
        plt.savefig(
            os.path.join(output_dir, "input_visualize",
                         os.path.basename(d["file_name"]))
        )


def visualize_result(out, img, metadata_dicts):
    """gets an instance and displays image"""
    vis = Visualizer(img[:, :, ::-1], scale=0.5, metadata=metadata_dicts)
    for mask in out["instances"].pred_masks.to("cpu"):
        vis.draw_binary_mask(mask.numpy())
    v = vis.get_output()
    img = v.get_image()[:, :, ::-1]
    return img
