import cv2
import json
from pycocotools.coco import COCO
import matplotlib.pyplot as plt
import sys
import os
import numpy as np
import albumentations as A

from PIL import Image
from PIL import ImageDraw


def transform(image, mask, how=1):
    if how == 1:
        transform = A.Compose(
            [
                # A.InvertImg(),
                # A.GaussNoise(p=0.6),
                A.RandomBrightnessContrast(p=0.2),
                A.Flip(),
                A.RandomRotate90(),
            ])
        return transform(image=image, mask=mask)

    elif how == 2:
        transform = A.Compose(
            [
                A.HorizontalFlip(p=1),
                A.RandomSizedCrop(min_max_height=(
                    int(int(height) * 0.8), int(int(height) * 0.9)), width=int(width), height=int(height)),
                A.GaussNoise(var_limit=(100, 150), p=0.6),

            ],
            p=1,
        )
        return transform(image=image, mask=mask)
    else:
        raise ValueError("how must be 1 or 2")


def find_contours(sub_mask):
    gray = cv2.cvtColor(sub_mask, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]


def create_annotation_format(contour, image_id, category_id, annotation_id):
    return {
        "iscrowd": 0,
        "id": annotation_id,
        "image_id": image_id,
        "category_id": category_id,
        "bbox": cv2.boundingRect(contour),
        "area": cv2.contourArea(contour),
        "segmentation": [contour.flatten().tolist()],
    }


def get_coco_annotations(mask, image_id, category_id, annotation_id):
    annotations = {}
    contours = find_contours(mask)
    for contour in contours:
        annotation = create_annotation_format(
            contour, image_id, category_id, annotation_id
        )
        annotations[annotation_id] = annotation
        annotation_id += 1
    return annotations


def visualize(img, mask, annotations, original_img):
    # convert img to PIL image "L"
    img = Image.fromarray(img)
    # convert to greyscale
    img_grey = img.convert("L")
    helper_img = Image.new("RGB", img_grey.size)
    helper_img.paste(img_grey)
    overlay_out = ImageDraw.Draw(helper_img, "RGBA")
    for k in annotations.keys():
        segs = annotations[k]["segmentation"][0]
        if len(segs) < 4:
            print("warning")
            continue
        overlay_out.polygon(segs, fill=(21, 239, 116, 27), outline="#B7FF00")
    # create plot with 3 columns that show image, augmented image and overlay
    fig, ax = plt.subplots(1, 3, figsize=(20, 10))
    ax[0].imshow(img)
    ax[1].imshow(helper_img)
    ax[2].imshow(original_img)
    ax[0].set_title("Augmented Image")
    ax[1].set_title("Overlay")
    ax[2].set_title("Original Image")
    return helper_img, plt


def augment(how=1):
    max_annot_id = max(coco.anns.keys()) + 1
    ids = list(coco.imgs.keys())
    max_id = max(ids) + 1
    key = ids[0]
    new_images = {}
    for key in coco.imgs.keys():
        if 'augmented' in coco.imgs[key]['file_name']:
            continue
        cur_entry = coco.imgs[key]
        entry_copy = cur_entry.copy()
        file_name = cur_entry["file_name"]
        cur_img = cv2.imread(os.path.join(img_dir, file_name))
        cv2.imwrite(os.path.join(out_dir, file_name), cur_img)
        cur_mask = cv2.imread(
            os.path.join(mask_dir, "masks", file_name.split(".")[0] + "_mask.png")
        )
        transformed = transform(image=cur_img, mask=np.array(cur_mask), how=how)
        transformed_img = transformed["image"]
        transformed_mask = transformed["mask"]
        augmented_img_name = file_name.split(".")[0] + f"_augmented{how}.{file_name.split('.')[1]}"
        augmented_mask_name = file_name.split(".")[0] + f"_augmented{how}_mask.png"
        entry_copy["file_name"] = augmented_img_name
        entry_copy["id"] = max_id
        new_images[max_id] = entry_copy
        annotations = get_coco_annotations(transformed_mask, max_id, 1, max_annot_id)
        max_id += 1

        # merge annotations
        coco.anns.update(annotations)
        coco.anns.keys()
        max_annot_id = max(annotations.keys()) + 1
        overlay, plt = visualize(
            transformed_img, transformed_mask, annotations, cur_img
        )
        plt.savefig(os.path.join(out_dir, "plots", augmented_img_name))
        plt.close()
        cv2.imwrite(
            os.path.join(out_dir, "masks", augmented_mask_name), transformed_mask
        )
        cv2.imwrite(os.path.join(out_dir, augmented_img_name), transformed_img)
        overlay.save(os.path.join(out_dir, "overlay", augmented_img_name))
        # cv2.imwrite(os.path.join(out_dir, 'overlay', augmented_img_name), np.array(overlay))
    # save coco json
    coco.imgs.update(new_images)
    json_dict = {"images": [], "annotations": [], "categories": []}
    for key in coco.imgs.keys():
        json_dict["images"].append(coco.imgs[key])
    for key in coco.anns.keys():
        json_dict["annotations"].append(coco.anns[key])
    for key in coco.cats.keys():
        json_dict["categories"].append(coco.cats[key])
    with open(os.path.join(out_dir, "annotations_aug.json"), "w") as f:
        json.dump(json_dict, f)

    # check if the coco json is valid
    coco2 = COCO(os.path.join(out_dir, "annotations_aug.json"))


if __name__ == "__main__":
    global coco
    global img_dir
    global mask_dir
    global out_dir
    global width
    global height
    # img_dir = sys.argv[1]
    width = sys.argv[1]
    height = sys.argv[2]
    base_dir = sys.argv[3]
    augment_data = bool(int(sys.argv[4]))

    width_height = f"{width}_{height}"
    img_dir = os.path.join(base_dir, "01preprocessed", width_height)
    mask_dir = os.path.join(base_dir, "02annotated", width_height)
    coco = COCO(os.path.join(base_dir, "02_5split", width_height, "train", "annotations.json"))
    out_dir = os.path.join(base_dir, "03augmented", width_height)
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    if not os.path.exists(os.path.join(out_dir, "masks")):
        os.makedirs(os.path.join(out_dir, "masks"))
    if not os.path.exists(os.path.join(out_dir, "plots")):
        os.makedirs(os.path.join(out_dir, "plots"))
    if not os.path.exists(os.path.join(out_dir, "overlay")):
        os.makedirs(os.path.join(out_dir, "overlay"))
    if augment_data:
        augment(how=1)
        augment(how=2)

    # copy masks file to out_dir
    import glob
    to_copy = glob.glob(os.path.join(mask_dir, "masks", "*.png"))
    print(f'len of to copy: {len(to_copy)}')
    for f in to_copy:
        os.system(f"cp {f} {out_dir}/masks")
#  1488  mkdir data/03augmented/1280_960/train
#  1489  mkdir data/03augmented/1280_960/test
#  1490  cp data/02_5split/1280_960/test/annotations.json data/03augmented/1280_960/test/
#  1491  cp data/02_5split/1280_960/train/annotations.json data/03augmented/1280_960/train/
# make the above commands here in pythonn
    if not os.path.exists(os.path.join(out_dir, "train")):
        os.makedirs(os.path.join(out_dir, "train"))
    if not os.path.exists(os.path.join(out_dir, "test")):
        os.makedirs(os.path.join(out_dir, "test"))
    os.system(f"cp {os.path.join(base_dir, '02_5split', width_height, 'train', 'annotations.json')} {os.path.join(out_dir, 'train')}")
    os.system(f"cp {os.path.join(base_dir, '02_5split', width_height, 'test', 'annotations.json')} {os.path.join(out_dir, 'test')}")
    # and also copy all images in 02_5split/widht_height/test to 03augmented/width_height/
    import json
    with open(os.path.join(base_dir, "02_5split", width_height, "test", "annotations.json")) as f:
        coco = json.load(f)
    for img in coco['images']:
        os.system(f"cp {os.path.join(img_dir, img['file_name'])} {os.path.join(out_dir)}")
    # and also copy all images in 02_5split/widht_height/train to 03augmented/width_height/
    with open(os.path.join(base_dir, "02_5split", width_height, "train", "annotations.json")) as f:
        coco = json.load(f)
    for img in coco['images']:
        os.system(f"cp {os.path.join(img_dir, img['file_name'])} {os.path.join(out_dir)}")
