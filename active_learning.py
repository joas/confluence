""" This is the main script for the active learning pipeline. It should be
univerally applicable to all our models. The script defines the active learning
step size and the initial training set. Then it calls the corresponding model
and trains it with the inital subset. The model will provide a csv file with
the predictions on the unknown data. The script will then select the next batch
of images based on the uncertainty score that we get from the model. The script
then updates the training set with the new batch and repeats the process until
the stopping criterion is met. In parallel, the script will also train a model
based on random sampling (matching the active learning step size)

his script will create a direcotry for each active learning step, containing
the input data, the model, the predictions, and the uncertainty scores. It will
also create a log file that keeps track of the model performance and the
training set size.

Later this data can be used to create plots of the model performance and the
training set size and we can compare active learning to random sampling.
i
"""


# ------------------------------------------------------------------------------
# IMPORTS
# ------------------------------------------------------------------------------
import os
import copy
import logging
import glob
import sys
import munch
import toml
import random
import time
from datetime import datetime
import shutil
from shutil import ignore_patterns
import pandas as pd
from pycocotools.coco import COCO
import json
import subprocess
import argparse


# ------------------------------------------------------------------------------
# GLOBAL VARIABLES (VALUES ARE PASSED BY TOML FILE)
# ------------------------------------------------------------------------------
global DATA_PATH
global TEMP_DATA_PATH
global STEP_SIZE
global INITIAL_TRAINING_SET
global INITIAL_TRAINING_SET_SIZE
global STOPPING_CRITERION
global MODEL_NAME
global MASTER_ANNOTATION_FILE
global MASTER_ANNOTATION_COCO
global RUN_ID
global SUBPROCESS_CALL
global USECASE
global RESUME
global logger
global RESULT_DIR_BASE


# ------------------------------------------------------------------------------
# FUNCTIONS
# ------------------------------------------------------------------------------


def overwrite_toml():
    """copies _config.toml to <RUN_ID>/config.toml and modifies the following
    parameters:
        - [data]
            train with: <TEMP_DATA_PATH>/train
            test with: <TEMP_DATA_PATH>/test
            pool with: <TEMP_DATA_PATH>/pool
        - [args]
            run_id: <RUN_ID>
        rest of the parameters are taken from the _config.toml file
    """
    if not os.path.exists(RESULT_DIR_BASE):
        os.makedirs(RESULT_DIR_BASE, exist_ok=True)
    shutil.copyfile("_config.toml", os.path.join(RESULT_DIR_BASE, "config.toml"))
    with open(os.path.join(RUN_ID, "config.toml"), "r") as f:
        data = toml.load(f)
    data["data"]["train"] = os.path.join(TEMP_DATA_PATH, "train")
    data["data"]["test"] = os.path.join(TEMP_DATA_PATH, "test")
    data["data"]["pool"] = os.path.join(TEMP_DATA_PATH, "pool")
    data["args"]["run_id"] = RESULT_DIR_BASE
    with open(os.path.join(RESULT_DIR_BASE, "config.toml"), "w") as f:
        toml.dump(data, f)


def update_master_annotation_file(keep_ids, filenames):
    """takes the MASTER_ANNOTATION_FILE and removes the annotations for the
    images in the keep_ids list. It also moves the images from the pool to the
    train folder. The master annotation file not updated in place, but a new
    the annotation.json file will be overwritten in the pool folder.
    ARGS:
        keep_ids (list): list of image ids that should be kept in the pool
        filenames (list): list of filenames that should be moved from the pool
    RETURNS:
        None

    """
    with open(MASTER_ANNOTATION_FILE) as f:
        data = json.load(f)
    data["annotations"] = [
        anno for anno in data["annotations"] if anno["image_id"] not in keep_ids
    ]
    data["images"] = [img for img in data["images"] if img["id"] not in keep_ids]
    with open(os.path.join(TEMP_DATA_PATH, "pool", "annotations.json"), "w") as f:
        json.dump(data, f)

    # move images from pool to train
    for img in filenames:
        if os.path.exists(os.path.join(TEMP_DATA_PATH, "pool", img)):
            shutil.move(
                os.path.join(TEMP_DATA_PATH, "pool", img),
                os.path.join(TEMP_DATA_PATH, "train", img),
            )


def update_annotations_for_resume(filenames):
    """takes a list of filenames that have already been used for training.
    These filenames will be removed from the pool/annotations.json file. and
    added to the train/annotations.json file. This will be done by filtering the
    master.json file, without changing it.

    ARGS:
        filenames (list): list of filenames that have already been used for
        training
    RETURNS:
        None

    """
    with open(MASTER_ANNOTATION_FILE) as f:
        data = json.load(f)
    logger.info(f'Number of images in master annotation file: {len(data["images"])}')

    train_data = data.copy()
    data["images"] = [
        img for img in data["images"] if img["file_name"] not in filenames
    ]
    train_data["images"] = [
        img for img in train_data["images"] if img["file_name"] in filenames
    ]

    # get image ids for the images that have already been used for training
    image_ids = [img["id"] for img in data["images"]]
    train_ids = [img["id"] for img in train_data["images"]]

    data["annotations"] = [
        anno for anno in data["annotations"] if anno["image_id"] in image_ids
    ]
    train_data["annotations"] = [
        anno for anno in train_data["annotations"] if anno["image_id"] in train_ids
    ]
    logger.info(
        f'Number of images in pool annotation file after filter: {len(data["images"])}'
    )
    logger.info(
        f'Number of images in train annotation file after filter: {len(train_data["images"])}'
    )
    with open(os.path.join(TEMP_DATA_PATH, "pool", "annotations.json"), "w") as f:
        json.dump(data, f)

    with open(os.path.join(TEMP_DATA_PATH, "train", "annotations.json"), "w") as f:
        json.dump(train_data, f)


def get_inital_training_set():
    """takes the MASTER_ANNOTATION_FILE and the INITIAL_TRAINING_SET and
    creates a new coco.json file with annotations only for the images in the
    INITIAL_TRAINING_SET
    """
    coco = MASTER_ANNOTATION_COCO
    filenames = []
    keep_ids = []
    image_ids = coco.getImgIds()

    if len(INITIAL_TRAINING_SET) == 0:
        logger.info("No initial training set provided. Using random sampling.")
        # select random ids
        keep_ids = random.sample(image_ids, INITIAL_TRAINING_SET_SIZE)
        for ids in keep_ids:
            filenames.append(coco.loadImgs(ids)[0]["file_name"])

    else:
        for ids in image_ids:
            cur_name = coco.loadImgs(ids)[0]["file_name"]
            if cur_name in INITIAL_TRAINING_SET:
                keep_ids.append(ids)
                filenames.append(cur_name)
    logger.info(
        f"We use the following images for the initial training set: {filenames}"
    )
    logger.info(f"Their ids are: {keep_ids}")

    with open(MASTER_ANNOTATION_FILE) as f:
        data = json.load(f)

    train_coco = {}
    train_coco["annotations"] = [
        anno for anno in data["annotations"] if anno["image_id"] in keep_ids
    ]
    train_coco["images"] = [img for img in data["images"] if img["id"] in keep_ids]
    train_coco["categories"] = data["categories"]
    if not os.path.exists(os.path.join(RESULT_DIR_BASE, "AL_LOGGING")):
        logger.info("Creating logging directory")
        os.makedirs(os.path.join(RESULT_DIR_BASE, "AL_LOGGING"))
    outfile = os.path.join(RESULT_DIR_BASE, "AL_LOGGING", "step_0.json")
    # mo

    # write coco file for logging and documentation purposes
    with open(outfile, "w") as f:
        json.dump(train_coco, f)
    # write one coco file for the model (will be overwritten in each step)
    with open(os.path.join(TEMP_DATA_PATH, "train", "annotations.json"), "w") as f:
        json.dump(train_coco, f)
    # update pool file by removing the images that are in the training set
    update_master_annotation_file(keep_ids, filenames)


def get_next_batch(STEP):
    """This functions reads in a csv file with the uncertainty scores for each
    image and selects the next batch of images based on the uncertainty score.
    It also creates a new coco.json file with the annotations for the next
    training step in the active learning process.
    """

    if USECASE == "al":
        logger.info(f"Running active learning for step {STEP}")
        scores_df = pd.read_csv(
            os.path.join(RESULT_DIR_BASE, f"step_{STEP}", "active_learning.csv")
        )
        # sort Descending, for SAM, because the higher the score, the more uncertain
        # sort ascending for Detectron2, because the lower the score, the more uncertain
        scores_df = scores_df.sort_values(
            by=["score"], ascending="-o" in SUBPROCESS_CALL
        )
        # get next images by stepsize
        # check if we have enough images left in the pool
        if len(scores_df) < STEP_SIZE:
            logger.info(
                f"We only have {len(scores_df)} images left in the pool. We use them all for the next batch."
            )
            next_images = scores_df["filename"].tolist()
            next_ids = scores_df["fileid"].tolist()
        else:
            next_images = scores_df["filename"].iloc[:STEP_SIZE].tolist()
            next_ids = scores_df["fileid"].iloc[:STEP_SIZE].tolist()
        logger.info(f"We add the following images for the next batch: {next_images}")
        logger.info(f"Their ids are: {next_ids}")
    master_coco = MASTER_ANNOTATION_COCO
    cur_train_coco = COCO(os.path.join(TEMP_DATA_PATH, "train", "annotations.json"))
    keep_ids = cur_train_coco.getImgIds()
    random_str = ""
    if USECASE == "random":
        random_str = "random_"
        logger.info("We use random sampling for the next batch")
        all_ids = master_coco.getImgIds()
        already_used_ids = cur_train_coco.getImgIds()
        fresh_ids = [x for x in all_ids if x not in already_used_ids]
        # check if there are enough images left
        if len(fresh_ids) < STEP_SIZE:
            logger.info(
                "There are not enough images left in the pool. We use all of them."
            )
            next_ids = fresh_ids
        else:
            next_ids = random.sample(fresh_ids, STEP_SIZE)
        logger.info(f"we selected the following ids: {next_ids}")
    filenames = []
    for ids in keep_ids:
        filenames.append(master_coco.loadImgs(ids)[0]["file_name"])
    for ids in next_ids:
        filenames.append(master_coco.loadImgs(ids)[0]["file_name"])
        keep_ids.append(ids)
    logger.info(f"We use the following images for the next training set: {filenames}")
    logger.info(f"Their ids are: {keep_ids}")
    with open(MASTER_ANNOTATION_FILE) as f:
        data = json.load(f)
    train_coco = {}
    train_coco["annotations"] = [
        anno for anno in data["annotations"] if anno["image_id"] in keep_ids
    ]
    train_coco["images"] = [img for img in data["images"] if img["id"] in keep_ids]
    train_coco["categories"] = data["categories"]
    # prevent logging bug: for 0th step we log the initial data set and the
    # first selected image in one logging step, since both is step 0a
    if not RESUME:
        STEP += STEP_SIZE
    if RESUME and STEP == 0:
        STEP += STEP_SIZE


    outfile = os.path.join(
        RESULT_DIR_BASE, "AL_LOGGING", f"step_{random_str}{STEP}.json"
    )
    # write coco file for logging and documentation purposes
    with open(outfile, "w") as f:
        json.dump(train_coco, f)
    # write one coco file for the model (will be overwritten in each step)
    with open(os.path.join(TEMP_DATA_PATH, "train", "annotations.json"), "w") as f:
        json.dump(train_coco, f)
    # update pool annotation file by removing the images that are in the training set
    update_master_annotation_file(keep_ids, filenames)


def prepare_data(DATA_PATH):
    """function that creates temporary input dir with timestamp and moves all
    images and masks for train to pooly

    """
    # create human readable timestamp
    timestamp = time.time()
    date_time = datetime.fromtimestamp(timestamp)
    date_time = datetime.fromtimestamp(timestamp)

    str_date_time = date_time.strftime("%d-%m-%Y-%H-%M-%S")
    if not os.path.exists(os.path.join(DATA_PATH, "pool")):
        os.makedirs(os.path.join(DATA_PATH, "pool", "imgs"))
        logger.info(f'created {os.path.join(DATA_PATH, "pool")}')

    temp_in_path = os.path.join(DATA_PATH, f"temp-{USECASE}_{str_date_time}")
    logger.info(f"created {temp_in_path}")
    shutil.copytree(
        DATA_PATH,
        temp_in_path,
        dirs_exist_ok=True,
        ignore=ignore_patterns("temp_*", "temp-*"),
    )
    logger.info(f"moving input data to {temp_in_path}")
    train_files = os.listdir(os.path.join(temp_in_path, "train"))
    # move all images and masks to pool/imgs and pool/masks
    for img in train_files:
        if img == "master.json":
            continue
        shutil.move(
            os.path.join(temp_in_path, "train", img),
            os.path.join(temp_in_path, "pool", img),
        )
    logger.info("moved imgs and masks to pool")
    return temp_in_path


def get_last_step(root_folder):
    # check in results folder for all folder that start with run_id
    files = os.listdir(root_folder)
    if len(files) == 0:
        logger.warning("no step completed, preparing inital training set")
        get_inital_training_set()
        return 0
    l = [int(f.replace(".json", "").split("_")[-1]) for f in files]
    last_step = max(l) if l else 0
    return last_step


def prepare_for_resume(laststep):
    logger.info("preparing for resume")
    last_completed_step = max(laststep - STEP_SIZE, 0)
    logger.info(f"last completed step: {last_completed_step}")
    # get all files that were already used for training, according to the last completed step
    random_str = "" if not USECASE == "random" else "random_"
    coco = COCO(
        os.path.join(
            RESULT_DIR_BASE,
            "AL_LOGGING",
            f"step_{random_str}{last_completed_step}.json",
        )
    )
    fileids = coco.getImgIds()
    filenames = [coco.loadImgs(x)[0]["file_name"] for x in fileids]
    logger.info(f"len of already used files: {len(filenames)}")
    # move all clean filenames from pool to train that are in filenames,
    # since these are the images that were already used for training in the last step
    # since we starting the prepare data step at first, we need to move all images from pool to train
    # because all images are in pool at the beginning
    # do the same for masks
    for img in filenames:
        mask = img.replace(".jpg", "") + "_mask.png"
        try:
            shutil.move(
                os.path.join(TEMP_DATA_PATH, "pool", img),
                os.path.join(TEMP_DATA_PATH, "train", img),
            )
        except Exception as e:
            # if the file is not in pool, we can ignore the error
            logger.info(e)
            logger.info(f"Could not move {img} from pool to train")
            pass
    # update the annotation file
    logger.info(
        "moved all files from pool to train that were already used for training"
    )
    logger.info("updating annotation file")
    update_annotations_for_resume(filenames)
    logger.info("updated annotation file")


def get_eval_metrics(i, random_str, out_dir):
    """function that calls the evaluate.py script to get the evaluation metrics"""
    gt_path = os.path.join(DATA_PATH,"test", "masks")
    pred_path = os.path.join(out_dir, "test", "masks")
    call = [
        "python",
        "evaluate.py",
        f"--gt-path={gt_path}",
        "--gt-ext=png",
        f"--pred-path={pred_path}",
        "--pred-ext=png",
        f"--run-id={out_dir}",
    ]
    logger.info(f"calling evaluate.py with {call}")
    subprocess.call(call)

def remove_folder(folder_path):
    try:
        shutil.rmtree(folder_path)
        logger.info(f"Folder '{folder_path}' successfully removed.")
    except OSError as e:
        logger.info(f"Error: {folder_path} : {e.strerror}")

def active_learning():
    """main function that runs the active learning pipeline"""

    laststep = (
        0 if not RESUME else get_last_step(os.path.join(RESULT_DIR_BASE, "AL_LOGGING"))
    )
    logger.info(f"Last step: {laststep} and RESUME: {RESUME}")
    if RESUME:
        logger.info("RESUME FLAG IS SET")
        prepare_for_resume(laststep)
    len_total_ids = len(MASTER_ANNOTATION_COCO.getImgIds())

    logger.info(f"Total number of images: {len_total_ids}")
    poolsize = len_total_ids - INITIAL_TRAINING_SET_SIZE
    logger.info(f"Poolsize: {poolsize} (images without initial training set)")
    range_limit = min(poolsize, STOPPING_CRITERION)
    logger.info(f"Range limit: {range_limit}")
    if not RESUME:
        get_inital_training_set()
        if not os.path.exists(os.path.join(f"{RESULT_DIR_BASE}")):
            logger.info(f"Creating directory {RESULT_DIR_BASE}")
            os.makedirs(os.path.join(f"{RESULT_DIR_BASE}"))

    random_str = ""
    if USECASE == "random":
        random_str = "random_"
    for i in range(laststep, range_limit, STEP_SIZE):
        logger.info(f"(Re)-Starting at step {i}")
        # create cur_results dir
        results_dir = os.path.join(RESULT_DIR_BASE, f"step_{random_str}{i}")
        os.makedirs(results_dir, exist_ok=True)
        if EVAL_ONLY:
            get_eval_metrics(i, random_str, results_dir)
            logger.info("EVAL ONLY")
            continue
        # here we need to call the actual training script of the model
        cur_call = SUBPROCESS_CALL.copy()
        # DETECTRON2 case
        if "-o" in cur_call:
            logger.info("adding output dir to call")
            cur_call.append(results_dir)
            cur_call.append(f"--rootdir={TEMP_DATA_PATH}")
        # SAM case
        else:
            configpath = os.path.join(RESULT_DIR_BASE, "config.toml")
            cur_call.append(f"--configfile={configpath}")
            cur_call.append(f"--al-step={random_str}{i}")
        logger.info(f"Calling  \n: {cur_call}")
        subprocess.run(cur_call)
        logger.info(f"Finished step {i}, getting eval metrics for this step")
        get_eval_metrics(i, random_str, results_dir)
        get_next_batch(i)


if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    # log stream handler to log to stdout
    handler = logging.StreamHandler(sys.stdout)
    logging.basicConfig(level=logging.INFO, format=log_fmt, handlers=[handler])

    # use argparse to parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--configfile",
        dest="filepath",
        type=str,
        required=True,
        help="path to config file",
    )
    parser.add_argument(
        "--resume",
        dest="RESUME",
        action="store_true",
        help="flag to resume active learning pipeline",
    )
    parser.add_argument(
        "--metarun",
        dest="METARUN",
        type=str,
        default="R0",
        required=False,
        help="Name of the root dir for all AL Runs, goes from R0 to R<n>",
    )
    parser.add_argument(
        "--num-exp",
        dest="NUM_EXP",
        type=int,
        default=10,
        required=False,
        help="Number of how many times the active learning should be repeated"
    )
    parser.add_argument(
        "--eval-only",
        dest="EVAL",
        action="store_true",
        default=False,
        required=False,
        help="Only run eval script when all other AL stuff ran already"
    )


    NUM_EXP = parser.parse_args().NUM_EXP
    EVAL_ONLY = parser.parse_args().EVAL
    filepath = parser.parse_args().filepath
    RESUME = parser.parse_args().RESUME
    METARUN = parser.parse_args().METARUN
    CFG = munch.munchify(toml.load(filepath))
    DATA_PATH = CFG.ARGS.DATA_PATH

    USECASE = CFG.ARGS.USECASE
    STEP_SIZE = CFG.ARGS.STEP_SIZE
    INITIAL_TRAINING_SET = CFG.ARGS.INITIAL_TRAINING_SET
    INITIAL_TRAINING_SET_SIZE = CFG.ARGS.INITIAL_TRAINING_SET_SIZE
    STOPPING_CRITERION = int(CFG.ARGS.STOPPING_CRITERION)
    MASTER_ANNOTATION_FILE = CFG.ARGS.MASTER_ANNOTATION_FILE
    master_base = copy.copy(MASTER_ANNOTATION_FILE)
    RUN_ID_BASE = CFG.ARGS.RUN_ID
    SUBPROCESS_CALL = CFG.ARGS.SUBPROCESS_CALL
    SUBPROCESS_CALL = SUBPROCESS_CALL.split(" ")
    TEMP_DATA_PATH = prepare_data(DATA_PATH)
    MASTER_ANNOTATION_FILE = os.path.join(TEMP_DATA_PATH, "train", master_base)
    MASTER_ANNOTATION_COCO = COCO(MASTER_ANNOTATION_FILE)
    if not "-o" in SUBPROCESS_CALL:
        overwrite_toml()
    last_run = 0
    if RESUME:
        folders = glob.glob(
            os.path.join(
                "../confluence-results/temp-results", RUN_ID_BASE, f"{RUN_ID_BASE}_*"
            )
        )
        l = [int(os.path.basename(x).split("_")[-1]) for x in folders]
        last_run = max(l) if l else 0

    for i in range(last_run, NUM_EXP):
        RUN_ID = f"{RUN_ID_BASE}_{i}"
        RESULT_DIR_BASE = os.path.join(
            "../confluence-results/temp-results/", RUN_ID_BASE, f"{RUN_ID}"
        )
        logger.info(f"starting with {RUN_ID}")
        active_learning()
        logger.info(f"final_res_path {RUN_ID_BASE}_{i}")
        logger.info(f"removing tempdata dir")
        shutil.rmtree(TEMP_DATA_PATH)
        TEMP_DATA_PATH = prepare_data(DATA_PATH)
        MASTER_ANNOTATION_FILE = os.path.join(TEMP_DATA_PATH, "train", master_base)
        MASTER_ANNOTATION_COCO = COCO(MASTER_ANNOTATION_FILE)


        
        project_path = os.path.join("/projects/p_scads_saxocell/")
        if not os.path.exists(project_path):
            logger.info("Leipzig case")
            continue # LEIPZIG CASE
        helper = RESULT_DIR_BASE.replace("../","")
        project_res = os.path.join(project_path,helper)
        try:
            shutil.copytree(RESULT_DIR_BASE, project_res)
        except Exception as e:
            logger.warning(f'could not copy files to {project_res}')
            logger.warning(e)
            pass

        RESUME = False
        # move output dir to results and add timestamp


    timestamp = time.time()
    # create human readable timestamp
    date_time = datetime.fromtimestamp(timestamp)
    str_date_time = date_time.strftime("%d-%m-%Y-%H-%M-%S")
    final_res_path = os.path.join(
        "../confluence-results/final-results/",
        METARUN,
        f"{RUN_ID_BASE}_{str_date_time}",
    )
    if not os.path.exists(final_res_path):
        os.makedirs(final_res_path, exist_ok=True)

    shutil.move(
        os.path.join("../confluence-results/temp-results/", RUN_ID_BASE), final_res_path
    )

    logger.info("moving slurmlogs to result dir")
    source_directory = "./"
    files_to_move = glob.glob(f"slurm_{RUN_ID_BASE}*")
    destination_directory = f"../confluence-results/final-results/{METARUN}/{RUN_ID_BASE}_{str_date_time}"
    for file_path in files_to_move:
        shutil.move(file_path, destination_directory)

    logger.info(
        f"Moved {os.path.join('../confluence-results/temp-results/',RUN_ID_BASE)}/* to {final_res_path}"
    )
    df = pd.read_csv(
        os.path.join(
            "..", "confluence-results", "final-results", METARUN, "respaths.csv"
        ),
        index_col=0,
    )
    df["path"][RUN_ID_BASE] = str(os.path.join(final_res_path, RUN_ID_BASE))
    df.to_csv(
        os.path.join(
            "..", "confluence-results", "final-results", METARUN, "respaths.csv"
        )
    )
    remove_folder(TEMP_DATA_PATH)
    logger.info("Finished active learning pipeline")

