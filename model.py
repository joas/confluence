from detectron2.engine import DefaultTrainer, DefaultPredictor
import argparse
import numpy as np
import random
from detectron2.data import MetadataCatalog, DatasetCatalog, DatasetMapper
from detectron2.data.datasets import register_coco_instances
import sys
import os
import cv2
import matplotlib.pyplot as plt
import json
import logging
import torch
from detectron2.data.build import build_detection_test_loader
from scipy.stats import entropy
import csv
from detectron2.evaluation import COCOEvaluator, inference_on_dataset
import pandas as pd
from detectron2.utils.logger import setup_logger
from utils.utils import (
    visualize_result,
    visualize_data,
    plt_acc_curve,
    plt_loss_curve,
    get_confluence,
    visualize_result2,
    setup_cfg,
)
from utils.loss_eval_hook import LossEvalHook

logger = logging.getLogger(__name__)

def seed_everything(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = True

class MyTrainer(DefaultTrainer):
    @classmethod
    def build_evaluator(cls, cfg, dataset_name, output_folder=None):
        if output_folder is None:
            output_folder = os.path.join(cfg.OUTPUT_DIR, "inference")
        return COCOEvaluator(dataset_name, cfg, True, output_folder)

    def build_hooks(self):
        hooks = super().build_hooks()
        hooks.insert(-1,LossEvalHook(
            5,
            self.model,
            build_detection_test_loader(
                self.cfg,
                test_name,
                DatasetMapper(self.cfg,True)
            )
        ))
        return hooks


def register_datasets(data_dict, dataset_dir, annot_file="annotations.json"):
    """lets detectron know about customd datasets
    Params:
        data_dict: dictionary of dataset names and paths
        dataset_dir: directory where datasets are stored
        annot_file: name of annotation file
    Returns:
        dataset_dicts: dictionary of dataset names and paths
        metadata_dicts: dictionary of dataset names and metadata

    """
    for k, v in data_dict.items():
        register_coco_instances(
            k,
            {},
            os.path.join(dataset_dir, v, annot_file),
            os.path.join(dataset_dir, v),
        )

    dataset_dicts = DatasetCatalog.get(train_name)
    metadata_dicts = MetadataCatalog.get(train_name)
    return dataset_dicts, metadata_dicts


def train(cfg, evaluate):
    trainer = MyTrainer(cfg)
    trainer.resume_or_load(resume=False)
    trainer.train()

    if evaluate:
        logger.warning("evaluation currently not implemented")
        # return
        logger.info("Done training, now start evaluation")
        evaluator = COCOEvaluator(test_name, False, output_dir=cfg.OUTPUT_DIR)
        val_loader = build_detection_test_loader(cfg, test_name)
        inference_on_dataset(trainer.model, val_loader, evaluator)
        metrics_df = pd.read_json(
            os.path.join(cfg.OUTPUT_DIR, "metrics.json"), lines=True, orient="records"
        )
        mdf = metrics_df.sort_values("iteration")
        plt_loss_curve(mdf, output_dir=cfg.OUTPUT_DIR)
        plt_acc_curve(mdf, output_dir=cfg.OUTPUT_DIR)


def predict(cfg, dataset_name):
    logger.info(f"predicting on {dataset_name}")
    if os.path.exists(os.path.join(cfg.OUTPUT_DIR, "model_final.pth")):
        logger.info('trained model exitst, loading weights')
        cfg.MODEL.WEIGHTS = os.path.join(cfg.OUTPUT_DIR, "model_final.pth")
    # set a custom testing threshold for this model
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0
    cfg.DATASETS.TEST = (test_name,)
    cfg.DATASETS.POOL = (pool_name,)
    predictor = DefaultPredictor(cfg)
    used_dataset_dicts = DatasetCatalog.get(dataset_name)
    test_metadata_dicts = MetadataCatalog.get(dataset_name)
    confluence_dict = {}
    predictions_out = []
    active_learning_dict = {"filename": [], "fileid": [], "score": []}
    teststring = "test" if (dataset_name == test_name) else ""
    for d in used_dataset_dicts:
        filename = d["file_name"]
        img_id = d["image_id"]
        logger.info(f"predicting on {filename}, datset_name: {dataset_name}")
        img = cv2.imread(filename)
        cur_pred = predictor(img)
        instances = cur_pred["instances"]
        scores = instances.scores
        # get entropy of scores
        avg_score = torch.mean(scores)
        # entr = entropy(scores.cpu().numpy())
        predictions_out.append(cur_pred)
        confluence, anns = get_confluence(d, cur_pred)
        if not os.path.exists(os.path.join(cfg.OUTPUT_DIR, teststring)):
            os.makedirs(os.path.join(cfg.OUTPUT_DIR, teststring))
        visualize_result2(d, anns, output_dir=os.path.join(cfg.OUTPUT_DIR, teststring))
        conf_perc = round(float(confluence), 4) * 100
        conf_perc = str(conf_perc) + "%"
        vis = visualize_result(cur_pred, img, test_metadata_dicts)
        plt.imshow(vis)
        plt.title(f"Confluence: Percentage of segmented pixels: {conf_perc}")
        out_name = d["file_name"].split("/")[-1].split(".")[0] + ".pred.png"
        plt.savefig(os.path.join(f'{cfg.OUTPUT_DIR}/{teststring}', out_name))
        plt.close()
        logger.info(f"saved plt to {out_name}")
        confluence_dict[d["file_name"]] = (confluence, "detectron2")
        active_learning_dict["filename"].append(filename)
        active_learning_dict["fileid"].append(img_id)
        active_learning_dict["score"].append(avg_score.item())

    with open(os.path.join(cfg.OUTPUT_DIR, "confluence.csv"), "w") as f:
        for key in confluence_dict.keys():
            f.write("%s,%s\n" % (key, confluence_dict[key]))
    if dataset_name == test_name:
        logger.info(
            f"returning, because predicting on test set, dataset_name: {dataset_name}"
        )
        return
    # only write active learning csv if not predicting on test set
    if dataset_name == pool_name:
        logger.info(
            f"writing active learning csv to {cfg.OUTPUT_DIR}/active_learning.csv"
        )
        with open(os.path.join(cfg.OUTPUT_DIR, "active_learning.csv"), "w") as f:
            # use keys as header for  csv columns
            w = csv.DictWriter(f, active_learning_dict.keys())
            w.writeheader()
            for i in range(len(active_learning_dict["filename"])):
                w.writerow(
                    {
                        "filename": active_learning_dict["filename"][i],
                        "fileid": active_learning_dict["fileid"][i],
                        "score": active_learning_dict["score"][i],
                    }
                )
            logger.info(
                f"wrote active learning csv to {cfg.OUTPUT_DIR}/active_learning.csv"
            )



def load_json_arr(json_path):
    lines = []
    with open(json_path, 'r') as f:
        for line in f:
            lines.append(json.loads(line))
    return lines


def main():
    parser = argparse.ArgumentParser(
        prog="Detectron2-Cell",
        description="Fine tunes detectron2 to work\
                                     cell data. Trains, evaluates and predicts\
                                     model",
        epilog="ScaDS.AI",
    )

    parser.add_argument("--rootdir", help="root directory of data", type=str)
    parser.add_argument("-t", "--train", action="store_true")
    parser.add_argument("-e", "--evaluate", action="store_true")
    parser.add_argument("-p", "--predict", action="store_true")
    parser.add_argument("-i", "--iter", default="20")
    parser.add_argument("-o", "--output_prefix", default="")
    parser.add_argument("-al", "--active_learning", action="store_true", default=False)
    args = parser.parse_args()
    basename = args.rootdir
    output_dir = args.output_prefix + ""

    global train_name
    global valid_name
    global test_name
    global pool_name
    global cfg
    train_name = basename + "_" + "train"
    test_name = basename + "_" + "test"
    valid_name = basename + "_" + "valid"
    pool_name = basename + "_" + "pool"
    data_dict = {
        train_name: "train",
        valid_name: "valid",
        test_name: "test",
        pool_name: "pool",
    }

    # LOGGING -----------------------------------------------------------------
    setup_logger()
    logger = logging.getLogger(__name__)
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    # log stream handler to log to stdout
    handler = logging.StreamHandler(sys.stdout)
    logging.basicConfig(level=logging.INFO, format=log_fmt, handlers=[handler])


    # CONFIG ------------------------------------------------------------------
    logger.info("start building config")
    cfg = setup_cfg(output_dir, int(args.iter))
    os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)
    dataset_dicts, metadata_dicts = register_datasets(data_dict, basename)
    cfg.DATASETS.TRAIN = (train_name,)
    cfg.DATASETS.TEST = (test_name,)

    # logger.info("visualizing input data")
    # visualize_data(dataset_dicts, metadata_dicts, output_dir=cfg.OUTPUT_DIR)

    # STARTING TRAINING OR PREDICTION -----------------------------------------
    if args.train:
        logger.info("training flag active")
        train(cfg, args.evaluate)

    if args.predict:
        logger.info("starting predictions")
        predict(cfg, test_name)
    if args.active_learning:
        predict(cfg, pool_name)


if __name__ == "__main__":
    seed = 100
    seed_everything(seed)
    main()
