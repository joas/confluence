import torch
import sys
import os
import glob
from torchvision.transforms import ToTensor

import rasterio
from rasterio.plot import show
import logging
import matplotlib.pyplot as plt
import cv2
from PIL import Image
import numpy as np

logger = logging.getLogger(__name__)


def get_confluence(image_path, save_image=True, title=True):
    """ Get the confluence of a raster image.
    Args:
        image_path (str): Path to the image.
    Returns:
        float: The confluence of the image.

    """
    with rasterio.open(image_path) as src:
        image_array = src.read(1)
        src.close()


    torch_image = ToTensor()(image_array)
    # replace .tif with .png in image_path
    background = (torch_image == 0.0000).sum()
    pixels = torch_image.shape[0] * torch_image.shape[1] * torch_image.shape[2]
    background_ratio = background / pixels
    confluence = 1 - background_ratio
    # conflunece to floatg
    confluence = float(confluence)
    confluence = round(confluence * 100, 2)
    # get confluence in %
    confluence = str(confluence) + "%"
    # round to 2 decimals
    if save_image:
        # save image_array as jpg not as plot

        ax = show(image_array)
        # get filename from image_path
        filename = os.path.basename(image_path)
        logger.info(f' working with {filename}')
        jpg_path = image_path.replace('.tif', '.jpg')
        jpg_path = jpg_path.replace('input','output')
        if title:
            ax.set_title(f'File: {filename} \n Confluence: {confluence}')
        # remove x axis
        ax.set_xticklabels([])
        # remove y axis
        ax.set_yticklabels([])
        # make axis invisible
        ax.axis('off')
        ax.figure.savefig(jpg_path, bbox_inches='tight',
                          pad_inches=0, transparent=True)
        logger.info(f'saved image to {jpg_path}')
        plt.close()

    # quality control get unique values
    unique, counts = torch.unique(torch_image, return_counts=True)
    logger.info(f"Unique values: {unique}")
    logger.info(f"Counts: {counts}")
    logger.info(f"Confluence: {confluence}")
    return confluence


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    in_path = sys.argv[1]
    tif_list = glob.glob('*.tif', root_dir=in_path)
    confluence_dict = {}
    out_path = in_path.replace('input', 'output')
    if not os.path.exists(out_path):
            os.mkdir(out_path)
    for f in tif_list:
        f = os.path.join(in_path, f)
        confluence = get_confluence(f, save_image=True, title=False)
        confluence_dict[os.path.basename(f)] = (confluence, 'labkit')
    # save confluence_dict to csv
    with open(os.path.join(out_path, 'confluence.csv'), 'w') as f:
        for key in confluence_dict.keys():
            f.write("%s,%s\n" % (key, confluence_dict[key]))
