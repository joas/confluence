### Intro
Firstly we need to annotate the images by hand. We recommend [ImgLab](https://solothought.com/imglab/) for this task. [Labelme](https://github.com/wkentaro/labelme) or [VGG](https://www.robots.ox.ac.uk/~vgg/software/via/via_demo.html) could be also used, but have limitations.

### Labelme
Labelme can be used via CLI, find the installation instructions [here](https://github.com/wkentaro/labelme). Note: it's best to use labelme in a virtual env because [this](https://stackoverflow.com/questions/71088095/opencv-could-not-load-the-qt-platform-plugin-xcb-in-even-though-it-was-fou/71918708#71918708) dependency error can occur.
You can do this by:

```bash
python -m venv labelme_env
pip install labelme_requirements.txt
source labelme_env/bin/activate
```


After annotating all images with labelme, we need to bring the annotations into the correct format (COCO format). Here we can use the script `labelme2coco.py` with:
 ```python
 python annotation_processing/labelme/labelme2coco.py <folder-with-labelme-annotations> <output-dir> --labels annotation_processing/labelme/labels.txt
 ```

NOTE: The labels.txt file may differ for your use case. As a result, you should get a folder with annotations in coco format like:
```
./cocoout/
|-- JPEGImages
|   |-- Aufnahme-01_0,5Mio_nach_24h1.jpg
|   `-- Aufnahme-01_0,5Mio_nach_24h.jpg
|-- Visualization
|   |-- Aufnahme-01_0,5Mio_nach_24h1.jpg
|   `-- Aufnahme-01_0,5Mio_nach_24h.jpg
`-- annotations.json

2 directories, 5 files

```
Then use this `annotation.json` file and copy it so that it fits the structure shown in [Training](#Training)
#### VGG
This is an online alternative to Labelme. We don't offer support/documentation for this tool, because importing projects/JSON files seems not to work. Thus, this is not feasible if you don't want to annotate everything at once.

#### ImgLab
ImgLab is an [online tool](https://imglab.in/#) for image annotation that makes it easy to upload work in progress via JSON files.
Simply follow these steps:
- upload your images via the file/folder symbol
- if you have already annotated some of them, press `<ctrl-i>` to import an existing annotations file. In our case, we use the COCO format
- (continue) annotation
- hit `<ctrl-e>` to export the project (again, we use COCO)
- save the coco.json file in `annotation_processing/ImgLab/input/`

This file will have duplicate annotations ids (unique for each image id, but not unique across all images), so we need to run the script `fix_json.py` to prevent errors from detectron2 later on.
Just run:
```python
python annotation_processing/ImgLab/fix_json.py annotation_processing/ImgLab/input/<your coco.json file>
```
This will save a correct annotations file in the shown [directory structure](#Training).

**Check the annotation JSON file**

If you worked with multiple people, there can be different categories in the JSON file:
![image.png](./image.png)

Fix this manually by deleting the unwanted categories and replacing the id, with the remaining id.
This will save a correct annotations file in the shown directory structure.
#### Labkit
Find all the information about [Labkit here](https://imagej.net/plugins/labkit/). Install the software as stated in the [docs]((https://imagej.net/plugins/labkit/)) and annotate the files also as stated in the docs.
Then click on Segmentation > save as tif and save the tif files to `annotation_processing/labkit/input/`

Then run:
```python
python annotation_processing/labkit/get_confluence.py annotation_processing/labkit/input/
```

This will create an output folder in `annotation_processing/labkit/` with all images as plots including the confluence as title and the file `confluence.csv` with the image filename and the confluence value.
