import json
import sys
import os



def fix_annos(path):
    """If annotations have 4 points they get confused with bounding boxes.
    This function adds a fake point to the annotation to make it a polygon.
    ARGS:
        path: path to json file
    RETURNS:
        None

    """
     # fix annotations:
    with open(path, 'r') as f:
        json_obj = json.load(f)
    for i, instance in enumerate(json_obj['annotations']):
        if len(instance['segmentation'][0]) <= 4:
            print(f' insance {i} has less than 4 points {instance["segmentation"][0]}')
            print(f'Adding fake points to instance {i}')
            fake_x = instance['segmentation'][0][0] + 1
            fake_y = instance['segmentation'][0][1] + 1
            instance['segmentation'][0].append(fake_x)
            instance['segmentation'][0].append(fake_y)
            print(f'new instance {i} has {instance["segmentation"][0]}')
    # save fixed annotations
    with open(path, 'w') as f:
        json.dump(json_obj, f)


def fix_json(file_path, outfile):
    """ creates unique annotations ids for a coco.json file with non-unique ids
    in our case the annotations ids start at 1 for each new image id.

    ARGS:
        file_path (str): path to coco.jso file
        outfile (str): path to outfile
    RETURNS:
        None

    """
    with open(file_path,"r") as f:
        data = json.load(f)
    print('makeing annos unique')
    for i in range(len(data['annotations'])):
        data['annotations'][i]['id'] = i+1
    with open(os.path.join(outfile), 'w+') as outfile:
        json.dump(data, outfile)




if __name__ == '__main__':
    file_path = sys.argv[1]
    print(file_path)
    fix_json(file_path, file_path)
    fix_annos(file_path)
    # paths = [os.path.join('data', 'train', 'annotations.json'),
    #          os.path.join('data', 'test', 'annotations.json'),
    #          os.path.join('data', 'valid', 'annotations.json')]

    # for path in paths:
    #     if not os.path.exists('data'):
    #         os.mkdir('data')
    #     if not os.path.exists(os.path.dirname(path)):
    #         os.mkdir(os.path.dirname(path))
    #     fix_json(file_path, path)
