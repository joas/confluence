import subprocess

for i in range(10):
    call = ['python', 'evaluate.py', '--gt-path=data/livecell/Instance_prep/LC_GENERALPREP_LAZY_data/test/masks', '--gt-ext=png', f'--pred-path=results/cp-lc-internallazy-rand_26-01-2024-13-10-33/cp-lc-internallazy-rand/step_random_{i}/test/predictions', '--pred-ext=png', f'--run-id=results/cp-lc-internallazy-rand_26-01-2024-13-10-33/cp-lc-internallazy-rand/step_random_{i}']
    subprocess.run(call)
