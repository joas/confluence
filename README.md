# Introduction
Here we explain how to run the Active Learning and finetuning/zero-shot processes for our paper "how to estimate confluence, lazily".
## Installation

- We recommend Python version 3.10.9
- create environment: `python3 -m venv confluence-env`
- activate: `source confluence-env/bin/activate`
- install torch (this is needed before we install the requirements, otherwise detectron2 cannot be installed): `pip install torch`
- install requirements: `pip install -r requirements.txt`

## Download data
You can find the processed data [here](https://cloud.scadsai.uni-leipzig.de/index.php/s/yrm6CmJMAJXgizT).
For details on the data processing see further [below](#motivation)

## Activle Learning
### Active Learning theory
This is an additional application of our models an might be moved to a seperate README in the future.
We use active learning (AL) to decide which images to label. Imagine you have a large data set of cell images an only limited budget to label. So it is not feasible to annoated the whole dataset. In this case you could just randomly selct X images and label them. Alternatively, you could use active learning, which will probably select a better training set.
The process works as follows:
- select an initial set of images to label (can be as small as 1 image for training)
- train a model on this images
- run inference on all unlabeled images with this model
- obtain an uncertainty score of how well the model can detect cells on each image
  - can be entropy of the output probabilites (High and low probabilites indicate mode is sure where we have cells, uniform probabilites indicate the model is unsure)
- rank the unlabeled images by the uncertainty
- label the X next images (can be as small as 1 image)
repeat process until labeling budget is reached (or goal accuracy)

### Active Learning implementation DETECTRON2 and SAM 
In our case we've already labeled all images that we want to use. We want to mimic the AL process and compare the model perfomances on models trained with images selected by AL and randomly selected ones.

To run this experiment you need to the the following:
1. create a `pool` directory next to the `train`
2. move all images from `train` to `pool`
3. create a copy if the `annotations.json` file and name it `master.json`
4. move the `annotations.json` file also to `pool`
5. prepare the `al_config.toml` file. Modify the following entries:
```
# Path to the data
# # FOR DETECTRON2 CONFIG --------------------------------------------------------
DATA_PATH = "LC_GENERALPREP/04cur_data/" # TODO change to your data
STEP_SIZE = 1 # how many images should be added in each AL step
INITIAL_TRAINING_SET = [] # leave empty, if the set should be selected randomly
INITIAL_TRAINING_SET_SIZE = 4 
STOPPING_CRITERION = 10 # how many activle learning steps should be perfomred
MASTER_ANNOTATION_FILE = "LC_GENERALPREP/04cur_data/train/master.json" # path to master.json
RUN_ID = "AL_TEST"
# don't change for detectron2 for SAM (different repo see below)
SUBPROCESS_CALL = "python model.py data/PIPELINETEST7/04cur_data/ -i 5 -t -p -al -o"
USECASE = "al" # al, or random are possible

# for SAM
# SUBPROCESS_CALL = "python src/main.py config.toml" # Subprocess call to start the training of the model
```
6. run the `active_learning.py`:

```python active_learning.py al_config.toml```
1. create a pool directory next to the imgs directory
1.1 Create the directories pool/imgs and pool/masks
2. move all images from imgs to pool/imgs and all masks form masks to pool/masks.
3. prepare the ```al_cp.toml``` file. The following need modification:
```
[ARGS]
[ARGS]
DATA_PATH = "LC_GENERALPREP" # Path to the data where the train, test and pool layers are located
STEP_SIZE = 1 # Number of images to be annotated in each AL iteration
INITIAL_TRAINING_SET = [] # List of images to be annotated in the first AL iteration, leave empty for random selection
INITIAL_TRAINING_SET_SIZE = 2 # only relevant if INITIAL_TRAINING_SET is empty, number of images to be randomly selected for the first AL iteration
STOPPING_CRITERION = 99 # Number of AL steps
RUN_ID = "cellposetest" # Name of the run
SUBPROCESS_CALL = "python cellpose_main.py --root-dir LC_GENERALPREP --evaluate --usegpu --active-learning --outpath"
USECASE = "al" # random or al (active learning)
```
4. run the `al_cp.py` script:

```
python al_cp.py al_cp.toml
```

### Active Learning implementation Cellpose & Unet

Note on uncertainty scores:
The detectron2 model outputs a vector of confidence scores for each detected instance [see here](https://detectron2.readthedocs.io/en/latest/tutorials/models.html?highlight=scores#model-output-format)
. We use the average of all scores as a metric for uncertainty for detectron2.

For SAM and Unet, we use the entropy of the probability map output of the models and a custom score:
The custom score is calculated as follows:
1. Obtain probability map from model output (width n_channel x height dimensional tensor)
2. Select a threshold for cell detection e.g probability > 0.5
3. Obtain binary mask with threshold and probability map
4. Calculate the pairwise difference between binary mask and the probabilites
5. Square the difference and build the sum. The idea is that a low difference beteen binary mask and probability indicates low uncertainty about the output.
# Old README for documentation
## Motivation

One objective of the SaxoCellSystem project is to estimate the cell confluence from microscopy images. Therefore, we need an automatic way to detect cells and then calculate the area covered by cells. We will use a pre-trained neural net for image detection. We chose [Facebooks Detectron2](https://github.com/facebookresearch/detectron2) for this.
Additionally, we apply U-net, Segment Anything (SAM), and Cellpose (TODO) to the problem. This repository focuses on the data preparation for all models and the fine-tuning of Detectron2, but we'll link to the repositories: [U-net](https://git.informatik.uni-leipzig.de/joas/confluence-unet/-/blob/main/create_dirs), [SAM](https://git.informatik.uni-leipzig.de/joas/confluence-sam) and [Cellpose](TODO) implementation are here and in the appropriate sections of this README.


Additionally, we provide information on how to annotate the cells with the GUI-based tool LabKit and provide scripts to get the confluence of the LabKit output.

## Install requirements

- We recommend Python version 3.10.9
- create environment: `python3 -m venv confluence-env`
- activate: `source confluence-env/bin/activate`
- install torch (this is needed before we install the requirements, otherwise detectron2 cannot be installed): `pip install torch`
- install requirements: `pip install -r requirements.txt`


## Obtain preprocessed data
We assume the data is already annotated and preprocessed. If not, check our preprocessing documentation [see here](https://git.informatik.uni-leipzig.de/joas/confluence/-/blob/main/Pipeline.md?ref_type=heads).
The data can be downloaded [here](https://zenodo.org/records/10350672?token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6IjdkOWFkN2RhLTVkZmEtNDRiMi1hYjIxLWQ5MDZkNThiMDlmOCIsImRhdGEiOnt9LCJyYW5kb20iOiIxZGRjOTQ5ODE0N2NmZDJkMjA3MGViZjc2ZmY1YTRlOCJ9.m5oaSwj1zYx1LDhAvFmiDkIJordrIcOO7GVJhz1ld4p4RMNOyzv-A06d3aNYFPbxP9cU8MyPIseMJ4JKtQ3bMQ). This folder contains three zipped folders:
- LC_GENERALPREP
- SC_GENERALPREP
- LC_EXT_GENERALPREP
- UNET_DATA

Firstly, you have to unzip the folders.
The ```UNET_DATA``` dir is an extra case and needed for step 10 later.
Each folder contains subfolders as shown in the file tree in the next section. The subfolders 00 - 03 contain intermediate results of the preprocessing and 04cur_data contains the dataset to use for our AI models. If you want to know more about the preprocessing steps, your use the raw data and do your own preprocessing please see [here](https://git.informatik.uni-leipzig.de/joas/confluence/-/blob/main/Pipeline.md?ref_type=heads)




## Pipeline Documentation


If you did all the preprocessing as in the preprocessing [README](https://git.informatik.uni-leipzig.de/joas/confluence/-/blob/main/Pipeline.md?ref_type=heads), you will end with a file tree as shown below. 
```
./data/
`-- PIPELINETEST7
    |-- 00raw
    |   |-- Image100.JPEG
    |   |-- Image117.JPEG
    |   |-- Image140.JPEG
    |-- 01preprocessed
    |   `-- 600_600
    |       |-- Image100.JPEG
    |       |-- Image117.JPEG
    |       |-- Image140.JPEG
    |-- 02_5split
    |   `-- 600_600
    |       |-- test
    |       |   `-- annotations.json
    |       `-- train
    |           `-- annotations.json
    |-- 02annotated
    |   `-- 600_600
    |       |-- masks
    |       |   |-- Image117_mask.png
    |       |   |-- Image140_mask.png
    |       |   |-- Image20_mask.png
    |       |-- vis
    |       |   |-- Image117_overlay.png
    |       |   |-- Image140_overlay.png
    |       |   |-- Image20_overlay.png
    |       `-- annotations.json
    |-- 03augmented
    |   `-- 600_600
    |       |-- masks
    |       |   |-- Image117_augmented1_mask.png
    |       |   |-- Image117_augmented2_mask.png
    |       |   |-- Image117_mask.png
    |       |   |-- Image140_augmented1_mask.png
    |       |   `-- Image29_mask.png
    |       |-- overlay
    |       |   |-- Image117_augmented1.JPEG
    |       |   |-- Image140_augmented2.JPEG
    |       |   `-- Image29_augmented2.JPEG
    |       |-- plots
    |       |   |-- Image117_augmented1.JPEG
    |       |   |-- Image140_augmented2.JPEG
    |       |   `-- Image29_augmented2.JPEG
    |       |-- test
    |       |   `-- annotations.json
    |       |-- train
    |       |   `-- annotations.json
    |       |-- Image117.JPEG
    |       |-- Image117_augmented1.JPEG
    |       |-- Image117_augmented2.JPEG
    |       |-- Image140.JPEG
    |       |-- Image140_augmented1.JPEG
    |       |-- Image140_augmented2.JPEG
    |       |-- Image29_augmented1.JPEG
    |       |-- Image29_augmented2.JPEG
    |       `-- annotations_aug.json
    `-- 04cur_data
        |-- test
        |   |-- Image20.JPEG
        |   `-- annotations.json
        `-- train
            |-- Image117.JPEG
            |-- Image140.JPEG
            |-- Image29.JPEG
            `-- annotations.json


    

```



IMPORTANT: The older README.md explained all steps from 1-7 to preprocess and annotate the images. This detailed explanation can be found [here](#detailPipeline.md) but was replaced by a single script [see also here](https://git.informatik.uni-leipzig.de/joas/confluence/-/blob/main/Pipeline.md?ref_type=heads). Now we assume, you either did the preprocessing, or downloaded the [preprocessed data](https://zenodo.org/records/10350672?token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6IjdkOWFkN2RhLTVkZmEtNDRiMi1hYjIxLWQ5MDZkNThiMDlmOCIsImRhdGEiOnt9LCJyYW5kb20iOiIxZGRjOTQ5ODE0N2NmZDJkMjA3MGViZjc2ZmY1YTRlOCJ9.m5oaSwj1zYx1LDhAvFmiDkIJordrIcOO7GVJhz1ld4p4RMNOyzv-A06d3aNYFPbxP9cU8MyPIseMJ4JKtQ3bMQ).

### Steps 1-7 (assuming data has already been annotated)
See [preprocessing pipeline](https://git.informatik.uni-leipzig.de/joas/confluence/-/blob/main/Pipeline.md?ref_type=heads), or skip, if you have downloaded the preprocessed data.

### Step 8 Run Detectron2 Model
The resulting directory `04cur_data` serves as input for the model so just run:
```
python model.py  data/livecell_external -i 20 -t -p -o External_test
```

```
usage: python model.py <input-dir> [-h] [-t] [-e] [-p] [-i ITER] [-o OUTPUT_PREFIX] 

Fine-tunes detectron2 to work cell data. Trains, evaluates, and predicts model

positional arguments:
  basename

options:
  -h, --help            show this help message and exit
  -t, --train
  -e, --evaluate
  -p, --predict
  -i ITER, --iter ITER
  -o OUTPUT_PREFIX, --output_prefix OUTPUT_PREFIX

ScaDS.AI
```
This will create an `output` directory with the following structure:
```
./output/
|-- Aufnahme-01_0,5Mio_nach_24h.pred.png
|-- Aufnahme-01_2Mio_nach_24h.pred.png
|-- ~cls_accuracy.png~
|-- ~coco_instances_results.json~
|-- confluence.csv
|-- events.out.tfevents.1674569915.scadsnb12.156512.0
|-- events.out.tfevents.1674573692.scadsnb12.163860.0
|-- instances_predictions.pth
|-- last_checkpoint
|-- ~loss_curve.png~
|-- metrics.json
|-- model_final.pth
`-- sample_data.png
```

### Step 9 Document
To make the results reproducible you can  archive the training and test data and the `output` directory like:
```
mkdir 999_ARCHIVE
mkdir 999_ARCHIVE/$(date +%F_%T)
cp -r ./data/04_cur_data ./999_ARCHIVE/<TIMESTAMP>
cp -r ./output ./999_ARCHIVE/<TIMESTAMP>
```
We also recommend keeping track of all runs in an Excel file as follows:
```
RUN_ID, DATE, TIMESTAMP, IN_DATA_PATHS, OUTPUT_PATHS, NOTES
```
see [here](https://cloud.scadsai.uni-leipzig.de/index.php/f/2274741)

### Step 10 Prepare for UNET
The UNET Model requires a slightly different input architecture. First, make sure that the required data structure is in place in the unet repo:
Alternatively, you can download a zip folder with the correct structure here:

- SC_GENERALPREP_data [For standard microscopy](https://zenodo.org/records/10350672?token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6IjdkOWFkN2RhLTVkZmEtNDRiMi1hYjIxLWQ5MDZkNThiMDlmOCIsImRhdGEiOnt9LCJyYW5kb20iOiIxZGRjOTQ5ODE0N2NmZDJkMjA3MGViZjc2ZmY1YTRlOCJ9.m5oaSwj1zYx1LDhAvFmiDkIJordrIcOO7GVJhz1ld4p4RMNOyzv-A06d3aNYFPbxP9cU8MyPIseMJ4JKtQ3bMQ)
- LC_GENERALPREP_data [For livecell external data](https://zenodo.org/records/10350672?token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6IjdkOWFkN2RhLTVkZmEtNDRiMi1hYjIxLWQ5MDZkNThiMDlmOCIsImRhdGEiOnt9LCJyYW5kb20iOiIxZGRjOTQ5ODE0N2NmZDJkMjA3MGViZjc2ZmY1YTRlOCJ9.m5oaSwj1zYx1LDhAvFmiDkIJordrIcOO7GVJhz1ld4p4RMNOyzv-A06d3aNYFPbxP9cU8MyPIseMJ4JKtQ3bMQ)
- LC_EXT_GENERALPREP_data [For livecell imaging internal data](https://zenodo.org/records/10350672?token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6IjdkOWFkN2RhLTVkZmEtNDRiMi1hYjIxLWQ5MDZkNThiMDlmOCIsImRhdGEiOnt9LCJyYW5kb20iOiIxZGRjOTQ5ODE0N2NmZDJkMjA3MGViZjc2ZmY1YTRlOCJ9.m5oaSwj1zYx1LDhAvFmiDkIJordrIcOO7GVJhz1ld4p4RMNOyzv-A06d3aNYFPbxP9cU8MyPIseMJ4JKtQ3bMQ)
```
./data/
|-- annotation
|-- imgs
|-- masks
|-- original
|   |-- imgs
|   `-- masks
`-- test
  |-- imgs
  `-- masks

```

This [script](https://git.informatik.uni-leipzig.de/joas/confluence-unet/-/blob/main/create_dirs.sh) in the unet repo will create the data structure


Now we can paste the input data to the unet input data structure:

The following script will copy the files from `./data/04_cur_data` to the corresponding structure:

``` 


python3 utils/prepare_data.py ./data/03augmented/1280_960/ ../confluence-unet/data/original/imgs/ ../confluence-unet/data/test/imgs/
python3 utils/prepare_data.py ./data/03augmented/1280_960/ ../confluence-unet/data/imgs/ ../confluence-unet/data/test/imgs/
 
```

ATTENTION: Be sure that in confluence-unet/data/imgs/ is NO annotations.json file, this will result in the following error: `AssertionError: Either no mask or multiple masks found for the ID annotations: []`
More general:
```
 python3 utils/prepare_data.py <INPATH> <UNET-TRAIN-PATH> <UNET-TEST-PATH>
```



### Step 11 Prepare and run Cellpose
[Cellpose](https://www.nature.com/articles/s41592-020-01018-x) is another model for instance segmentation. The model is based on Unet and optimized for cell segmentations. To use this model do the following:
- The input data has to be in the same structure as for the Unet model (see above - step 10).
- sample model call would be: ```python cellpose_main.py --root-dir LC_GENERALPREP --outpath MY_OUT_PATH --evaluate --usegpu --active-learning```
- To run the actual model you have these options:
```
usage: python cellpose_main.py [-h] [--outpath OUTPATH] [--active-learning] [--root-dir ROOT_DIR] [--evaluate] [--usegpu]

options:
  -h, --help           show this help message and exit
  --outpath OUTPATH    output path
  --root-dir ROOT_DIR  root directory of data
  --active-learning    optional, when active learning should be used
  --evaluate           optional, when the model should be evaluated on test data
  --usegpu             optional, use when model should run on GPU
  ```



### Step 12 Prepare for SAM
You can just copy the input data of Detectron2. For details see the [confluence-sam repo](https://git.informatik.uni-leipzig.de/joas/confluence-sam).

### Step 13 Evaluate

To obtain the "pixel accuracy" and "intersection over union" metrics, run the evaluation script with the following arguments:

Specific for Detectron2 in this example:

```
python evaluate.py data/03augumented/1280_960/masks/ png output/masks/ png DETECTRON
```

Specific for Unet in this example:

```
python evaluate.py ../confluence-unet/data/test/masks/ png  ../confluence-unet/results/predictions/test/ png UNET
```

General:

```
python evaluate.py <path-to-groundtruth-mask> <file-extension>  <path-to-predicted-mask> <file-extension> <identifier-string>
```

### Step 14 Extra Rescale Annotated Data
In some cases, it may be necessary to downscale the data, for some models. If the images were already annotated the annotations need to be rescaled too.
To accomplish this, we first scale the raw data into the desired format:

```
python3 utils/rescale.py data/00raw/ jpg 128 128 data/01preprocessed/
```
Secondly, we format the mask (we assume that you have done Steps 1-9 once already):
```
python3 utils/rescale.py data/02annotated/1280_960/masks/ png 128 128 data/02annotated/
mkdir data/02annotated/128_128/masks
mv data/02annotated/128_128/*.png  data/02annotated/128_128/masks/

 
```
Now we need to create an annotations.json file from the masks, then we can continue with Step 4 of this pipeline. therefore run:
```
 python3 utils/mask_to_coco.py ./data/02annotated/128_128/masks/ ./data/02annotated/128_128/
 ```

Now you can start at Step 4 again like:
```
python3 utils/coco_to_mask.py data/01preprocessed/128_128/ data/02annotated/128_128/annotation.json
```


## Step 15 Visualize results
#### AL vs Random results
We have now:
- 32 result folders for our Active learning experiment
  - 4 models times 4 datasets time 2 (one for active learning and one for random)
  - We want to have three types of plot that summarizes our results. The plots can be seen as drafts [here](https://docs.google.com/presentation/d/1V1BW7s82uu-jGTSVrUG11qbA3trVS8yy_1OuIx3RLFc/edit#slide=id.g1f1b55a22c7_0_0)
    - plot type1: al vs random. Here we plot the performance (IoU or delta confluence) for each step in the active learning process for the random and activle learning runs.
    - plot type2:AL in movie context. Here we plot the movie position against the AL position
    - plot type3: Goal dependent labellling here, we compare the lazy and non-lazy labeled data for IoU (favors exact labeling) and delta Confluence (does not care about exact labeling).



To genereate the plots we nee do run the following scripts.
```bash
python analuse_res.py <resultpath AL Run1>, <resultpath corresponding Random Run1>, ... <resultpath AL RunN>, <resultpath corresponding Random RunN>
```
You can find the structure to generate those runs and script calls in this [sheet](https://docs.google.com/spreadsheets/d/17VrC_gHNiUvYN1dzoHK_zxtUer33ZLIoTW-RQTgJfTc/edit?pli=1#gid=93726756)

#### Results without AL
We run all our models and all our data with more epochs than in the AL experiments. Here we compare our 4 models wiht our 4 datasets, so we have 16 different results. We get one plot per metric (IoU, delta Confluence) that combines all 16 results in one barplot. To generate these plots you need to have the following hardcoded structure (for now) in place.

```
./root/
|-- confluence
|   |--results_normal
|   |  |--<run_id_folder>
|   |  |  `--eval_metrics.csv
|-- confluence-unet
|   |--results_normal
|   |  |--<run_id_folder>
|   |  |  `--eval_metrics.csv
|-- confluence-sam
|   |--results_normal
|   |  |--<run_id_folder>
|   |  |  `--eval_metrics.csv

```

To run the script just run:
```
python confluence/ana_normal.py
```

```
`./root
|-- NORMAL_CONFLUENCE_AGGREGATED/
|   |-- absDelConf_by_model_and_data.png
|   |-- absDelConf_by_model_and_data_wError.png
|   |-- iou_by_model_and_data.png 
|   |-- iou_by_model_and_data_wError.png 
|   |-- relDelConf_by_model_and_data.png 
|   |-- relDelConf_by_model_and_data_wError.png 
```




## Webapp
To start the web app: run.
```bash
streamlit run app.py --<unet-model> <detectron2-model> --theme.base light --theme.primaryColor blue
```

The `test` dataset is used to make the `*.pred.png` as well as the `confluence.csv` and the `model_final.pth` comes from the `train` data

## Troubleshooting
- Make sure you have only 1 category_id per category in your annotations.json and start the id with 1



## 999 EXTRA: Compare with Labkit
To compare labkit with the performance of your other models, we need to do some extra steps to get the pixel accuracy and intersection over union.

Be sure you already have done step 07 and have the folder 04cur_data available. If you work on a cluster copy this data to your local machine. Be also sure that you're familiar with Labkit. See our [documentation](https://git.informatik.uni-leipzig.de/joas/confluence/-/blob/main/annotation_processing/README_ANNO_TOOLS.md#labkit). 

- Open a training data file from 04cur_data/train in Labkit and annotate the image.
- then click on train classifier in labkit
- now that we trained the classifier (unfortunately, we can not train the classifier for multiple images easily or at all see [here](https://forum.image.sc/t/how-to-train-labkit-on-multiple-images/55991/6) ), we want to use the classifier to segment all test images
-Therefore, create the output folder, where you want to save the segmentation results:
  - `mkdir LABKIT<NameofImage>`
  - `mkdir LABKIT<NameofImage>/input`
- Now batch segment the images with the current classifier and select the created folder as output
- Now we need to generate binary masks from the labkit segmentations. Therefore we use the following scripts:
``` bash
python annotation_processing/labkit/get_confluence.py <path-to-.tif files>
python annotation_processing/labkit/jpg_to_mask.py <path-to-output of step before>
```
Next, we need to scale the segmentation masks from labkit, so that we have the same format as our mask in the test data:

```
 python utils/rescale.py <labkit-mask-folder> png <width> <height> <output-folder>
```
Lastly, we run our evaluation script to get pixel accuracy and intersection over union:
```

python evaluate.py <path-gt-masks>  <mask-extension> <path-to-predicted-masks> <mask-extension> LABKIT
 ```
 Now we can check `LABKIT_results.csv`
