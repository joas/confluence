## 1 Introduction
This document serves two purposes:
Firstly, it documents how to get from raw image data, to preprocessed and annotated data to use in our AI models. 

Secondly, we briefly explain the current data structure and preprocessing of the data that is actually in use at the moment.

## 2 Requirements.
You can use this repo in three different ways:
Use our preprocessed, annotated data. Therefore, you can skip this document and refer to the [README.md of this repo](https://git.informatik.uni-leipzig.de/joas/confluence/-/blob/main/README.md?ref_type=heads)
You can use our raw data and our annotations and use this very document to perform your own preprocessing (maybe use a different image size, train/test split, and data augmentation than we did) TODO add a link
You can use your own data and preprocess it with the help of this very document. If you need to annotate the data, you can check our [README for annotations tools](https://git.informatik.uni-leipzig.de/joas/confluence/-/blob/main/annotation_processing/README_ANNO_TOOLS.md)

## Data Preparation Pipeline
This instruction manual documents the whole data process from obtaining the images for the wet lab researcher to running the AI models.
In the end, we will have created the following file structure:

```
./data/
`-- PIPELINETEST7
    |-- 00raw
    |   |-- Image100.JPEG
    |   |-- Image117.JPEG
    |   |-- Image140.JPEG
    |-- 01preprocessed
    |   `-- 600_600
    |       |-- Image100.JPEG
    |       |-- Image117.JPEG
    |       |-- Image140.JPEG
    |-- 02_5split
    |   `-- 600_600
    |       |-- test
    |       |   `-- annotations.json
    |       `-- train
    |           `-- annotations.json
    |-- 02annotated
    |   `-- 600_600
    |       |-- masks
    |       |   |-- Image117_mask.png
    |       |   |-- Image140_mask.png
    |       |   |-- Image20_mask.png
    |       |-- vis
    |       |   |-- Image117_overlay.png
    |       |   |-- Image140_overlay.png
    |       |   |-- Image20_overlay.png
    |       `-- annotations.json
    |-- 03augmented
    |   `-- 600_600
    |       |-- masks
    |       |   |-- Image117_augmented1_mask.png
    |       |   |-- Image117_augmented2_mask.png
    |       |   |-- Image117_mask.png
    |       |   |-- Image140_augmented1_mask.png
    |       |   `-- Image29_mask.png
    |       |-- overlay
    |       |   |-- Image117_augmented1.JPEG
    |       |   |-- Image140_augmented2.JPEG
    |       |   `-- Image29_augmented2.JPEG
    |       |-- plots
    |       |   |-- Image117_augmented1.JPEG
    |       |   |-- Image140_augmented2.JPEG
    |       |   `-- Image29_augmented2.JPEG
    |       |-- test
    |       |   `-- annotations.json
    |       |-- train
    |       |   `-- annotations.json
    |       |-- Image117.JPEG
    |       |-- Image117_augmented1.JPEG
    |       |-- Image117_augmented2.JPEG
    |       |-- Image140.JPEG
    |       |-- Image140_augmented1.JPEG
    |       |-- Image140_augmented2.JPEG
    |       |-- Image29_augmented1.JPEG
    |       |-- Image29_augmented2.JPEG
    |       `-- annotations_aug.json
    `-- 04cur_data
        |-- test
        |   |-- Image20.JPEG
        |   `-- annotations.json
        `-- train
            |-- Image117.JPEG
            |-- Image140.JPEG
            |-- Image29.JPEG
            `-- annotations.json

```


In an earlier version of this document, you had to perform all preprocessing steps separately. Now, we've added a bash script `main_preprocessing.sh` that will perform steps 1-7. You need to have a zip folder with all the raw data and the annotation file in COCO format. You have to provide the following arguments:
```bash
# ARGUMENTS ------------------------------------------------------------------
indata=$1 # path of zip folder with all images
run_id=$2 # unique id for this run
width=$3 # width of images
heigth=$4 # heigth of images
augment=$5 # Should augmentation be performed? 1 = yes, 0 = no
split_size=$6 # size of train set in 0.0-1.0
annot_file=$7 # The path to the annotation file should be in root
file_ext=$8 # file extension of images
```

This is an example call of the main_preprocessing script:
```
  ./main_preprocessing.sh Movie2.zip PIPELINETEST7 600 600 1 0.8 2023-07-18_09-57-38_7_coco_imglab.json JPEG
```

If you are interested in each step that the bash script performs, see the whole pipeline at the end of the file (note, you don't need to do this step-by-step. This part is only for documentation and maybe troubleshooting purposes)

### Current Data Preprocessing.
The data can be downloaded here. This folder contains three zipped folders:
LC_GENERALPREP
SC_GENERALPREP
LC_EXT_GENERALPREP
UNET_DATA
The LC_GENERALPREP data contains of 19 images from „Live Cell Imaging“, with a CytoSmart Lux (10-fold zoom; 5 megapixel-camera) and the preprocessing script was run with the following arguments:

```
/work/users/me792rqay/imagestudy/datahub/livecell/data.zip LC_GENERALPREP 1280 960 0 0.9 annotations.json  jpg
```

The SC_GENERALPREP consists of 19 images from standard microcopsy images taken with a ZEISS Axiovert 40 CFL, Objektiv 10X Ph1 (Phasenkontrast); Axiocam ERc 5s (5 Megapixel) and the preprocessing scrip was run with the following arguments:

```
/work/users/me792rqay/imagestudy/datahub/standardmicro/scdata.zip SC_GENERALPREP 512 512 0 0.9 2023-09-19_23-11-36_annotations_03.json  jpg
```

The LC_EXT_GENERALPREP data comes directly from [this study](https://www.nature.com/articles/s41592-021-01249-6) and has not been further preprocessed, since this has already been done by the authors. 

### Old Pipeline steps
### Step 1 Obtain Raw Data and install requirements
If the directory does not exist create the `data` and `00raw` directories with:
```bash
mkdir data
mkdir data/00raw
```
- create environment: `python3 -m venv confluence-env`
- activate: `source confluence-env/bin/activate`
- install torch (this is needed before we install the requirements, otherwise detectron2 cannot be installed): `pip install torch`
- install requirements: `pip install -r requirements`

Our wet lab partners usually save the raw data [here](https://cloud.scadsai.uni-leipzig.de/index.php/f/1600730)

If you work on our HPC, the data can be stored as follows:

- upload the raw files to the cluster with ```scp -r Images.zip me792rqay@login01.sc.uni-leipzig.de:/work/users/me792rqay```
- Step 2 login to cluster and unzip:
```bash
ssh me792rqay@login03.sc.uni-leipzig.de
unzip <Images.zip>
mv Images/*.fileext /work/users/me792rqay/microscopy/00raw
```

### Step 2 Rescale Raw Data


- run the preprocessing (rescale and remove metadata) with:
```bash
python3 utils/rescale.py data/00raw/ jpg 1280 960 data/01preprocessed/
```
- more general:
```
python3 utils/rescale.py <inpath> <file format> <width> <height> <outpath>
```
this crates a folder named `<HEIGHT>_<WIDTH>` in `<OUTPATH>` in our case `01preprocessed/1280_960` containing all the rescaled images. So the structure looks as follows:
```
-- data
    |-- 00raw
    |   `-- Aufnahme-01_0,5Mio_nach_24h.jpg
    |-- 01preprocessed
    |   `-- 1280_960
    |       `-- Aufnahme-01_0,5Mio_nach_24h.jpg
```
### Step 3 Annotate

  - upload the files from `data/01preprocessed/1280_960` to an annotation tool ([see here for all tools](https://git.informatik.uni-leipzig.de/joas/confluence/-/blob/main/annotation_processing/README_ANNO_TOOLS.md)), here we use ImageLab:
  
> *ImgLab*\
> ImgLab is an online tool for image annotation that makes it easy to upload work in progress via JSON files.
> Simply follow these steps:
>
> - upload your images via the file/folder symbol
> - if you have already annotated some of them, press <ctrl-i> to import an existing annotations file. In our case, we use the COCO format
> - (continue) annotation
> - Hit <ctrl-e> to export the project (again, we use COCO)
> - save the coco.json file in `data/02annotated/<WIDTH>_<HEIGHT>`
> - create the directory if necessary

 This file will have duplicate annotation ids (unique for each image id, but not unique across all images), so we need to run the script fix_json.py to prevent errors from detectron2 later on.

Just run:
```
python3 annotation_processing/ImgLab/fix_json.py <your coco.json file>
```
Check the annotation JSON file
If you worked with multiple people, there can be different categories in the JSON file:
![image-1.png](./image-1.png)

Fix this manually by deleting the unwanted categories and replacing the id, with the remaining id. Example Replace category_id": 2 with category_id": 1 and so on.

### Step 4 Visualize and create masks

In this step, we will create binary masks from the annotations and overlay visualizations. Run:
``` 
 python3 utils/coco_to_mask.py data/01preprocessed/1280_960/ data/02annotated/1280_960/annotations.json
 ```
 Now our structure looks as follows:
```
-- data
    |-- 00raw
    |   `-- Aufnahme-01_0,5Mio_nach_24h.jpg
    |-- 01preprocessed
    |   `-- 1280_960
    |       `-- Aufnahme-01_0,5Mio_nach_24h.jpg
    |-- 02annotated
    |   `-- 1280_960
    |       |-- masks
    |       |   `-- Aufnahme-01_0,5Mio_nach_24h_mask.png
    |       |-- vis
    |       |   |-- Aufnahme-01_0,5Mio_nach_24h_overlay.png
    |       |-- Aufnahme-01_0,5Mio_nach_24h_mask.png
    |       `-- annotations.json
```
### Step 5 Augment Data
In this step, we create 2 augmented images for every 'real' image. We perform the same transformations for the masks, so we don't have to annotate the augmented images. Lastly, we need to create a new annotations.json file containing the coco annotations from the real and augmented images. Run:
 ```
 python3 utils/augment.py <WIDTH> <HEIGHT>
 ```

### Step 6 Creating Train/Test Split

To split the data run the following for the described setup with a train test split of 0.9/0.1.

``` python3 utils/cocosplit.py -s 0.9 ./data/03augmented/1280_960/annotations_aug.json  ./data/03augmented/1280_960/train/annotations.json ./data/03augmented/1280_960/test/annotations.json```

More general:
```
python3 utils/cocosplit.py -s <trainsize> <input_coco_file> <outpath_train> <outpath_test>
```

### Step 7 Prepare for Models
Lastly, we prepare the data, so we can use the `model.py` script for training. Therefore we just run:

```
python3 utils/prepare_data.py ./data/03augmented/1280_960/ ./data/04cur_data/train ./data/04cur_data/test
```

More general:

```
python3 utils/prepare_data.py <INPATH> <OUTPATH_TRAIN> <OUTPATH_TEST>
```
